Kayamb
======



requirements
============

* Maven
* ~~RoleConverter native lib~~ 
* mongodb information located ```/opt/Sati/db.json```, an example ca be found ```./src/main/resources/data/db.json```

setup
=====

To run:

1. Open a terminal window

2. Navigate to the root directory of the project (where the pom.xml is)

3. ~~'mvn process-classes -P Unix' compile native lib~~

4. 'mvn clean package' (this will compile and build the project)

5. ~~'mvn exec:java' or~~ 'mvn exec:exec'
	
TODO
====

Add request to destroy rule

~~add security when db not present~~

~~use db.json to get mongo db connection info~~

~~Check copy construtors for BatteryLevel, CommFailure~~

~~Define better management for Loads of Default rule and saved rules~~ Rules saved/loaded from mongo database

~~Add specialized rule to replace global one to a specific user~~ Rules are global and specialized by specifying a user

~~Have better generic rule result output~~ Result is saved to a mongo database and send to a server to be processed

Control memory and cpu consumption -> one esper instance

~~evaluate pom.xml to exec:java exec:exec~~ definitely exec:exec

DONE
====

~~Can use global OR specific rules, use global and specialize each rule by using user parameter in each rule for now~~

~~Make file write in separated thread linked to each user (using Files class as singleton an synchronize) it seems to be well working~~

Re enable timestamp in ms for role converting

~~add persistance to rules, and check already persisted new rules~~

~~add multiple rules by file managing~~


IMPORTANT
=========

Unifying Motion and contact semantics in RoleOneEvent native lib

1. there is:

Motion.status=true as the begining of a motion

Motion.status=false as the end of a motion

Contact.status=false as the opening

Contact.status=true as the closing

2. what is done:

Motion.status=true -> Presence.status=true

Motion.status=false -> Presence.status=false

~~Contact.status=false -> Opening.status=true~~

~~Contact.status=true -> Opening.status=false~~

Contact.status=1 -> Opening.status=open

Contact.status=0 -> Opening.status=false


TIPS
====

How do I report at a regular interval without any incoming events?

    Let's say we want to have our listener get invoked every 5 seconds, and select the last value, if any, from a stream.

    select (select price from MarketData.std:lastevent()) as price
    from pattern [every timer:interval(5 sec)]

    The pattern fires every 5 seconds causing the sub-select to take place, returning null if no MarketData events have come in, or returning the price column of the last MarketData event.


delete rule:

    Method DELETE http://localhost:8080/rule/test_win


set rule:

    Method PUT http://localhost:8080
    body:
    {
	"source": "rule",
	"type": "rule",
	"name": "departure_alert",
	"force_write": "true",
	"expression": ["select Calendar_Night_b,Door_Entrance_open from pattern [ Calendar_Night_b=StreamEvent(role.location='Night',role.type='Calendar',status!='end') -> Door_Entrance_open=StreamEvent(role.location='Entrance',role.type='Door',status='open',user=Calendar_Night_b.user) -> timer:interval(300msec) and not ( StreamEvent(role.location='Entrance',role.type='Door',status='close',user=Calendar_Night_b.user) ) and not ( StreamEvent(role.location='Night',role.type='Calendar',status='end',user=Calendar_Night_b.user) )  ]"]
	}




DATA
====
