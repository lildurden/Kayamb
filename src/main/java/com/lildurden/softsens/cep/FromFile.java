package com.lildurden.softsens.cep;

import com.lildurden.softsens.cep.event.manager.DispatchRole;
import com.lildurden.softsens.cep.handler.RoleEventHandler;
import com.lildurden.softsens.cep.subscriber.manager.RuleManager;
import com.lildurden.softsens.cep.subscriber.manager.Rule;
import com.lildurden.softsens.cep.util.inout.Output;
import com.lildurden.softsens.cep.util.inout.MongoWrite;
import com.lildurden.softsens.cep.util.DoCalendar;
import com.lildurden.softsens.cep.http.Decode;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jetty.server.handler.AbstractHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.io.BufferedReader;
import java.util.ListIterator;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.File;
import java.io.FileReader;
import java.util.List;
import java.util.stream.Collectors; 

import java.text.SimpleDateFormat;
import java.text.DateFormat; 
import java.util.Date;
import java.sql.Timestamp; 

// import java.util.Calendar;
// import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.lang.InterruptedException;

public class FromFile {
    private static Logger LOG = LoggerFactory.getLogger(FromFile.class);
    
    // private HttpServletRequest request;
    // private HttpServletResponse response;
    private Map<String,DispatchRole> dispatchRoles;
            
    private RoleEventHandler roleEventHandler;

    private DoCalendar docal=new DoCalendar();

    public FromFile(RoleEventHandler roleEventHandler){
	this.roleEventHandler=roleEventHandler;
	dispatchRoles=new HashMap<String,DispatchRole>();
	LoadRules();
    }
    public FromFile(){
	roleEventHandler=new RoleEventHandler();
	LOG.info("getEventfromfiles");
	dispatchRoles=new HashMap<String,DispatchRole>();
	LoadRules();
	//Thread t = new Thread(new RunImpl("/home/lildurden/Documents/these/Apt1-2017-full.json/Apt1-2017-full.json"));

	//Thread t = new Thread(new RunImpl("/home/lildurden/Documents/these/suite/fullSort.json"));
	
	//Thread t0 = new Thread(new RunImpl("/home/lildurden/Documents/these/suite/full/full/fullS.json"));
	// Thread t0 = new Thread(
	// 		       new RunImpl("/home/lildurden/Documents/these/suite/full/full/45107798-2017.json"));
	RunImpl impl=new RunImpl("/home/lildurden/Documents/these/suite/full/full/45107798-2017.json");
	impl.run();
	//Thread t0 = new Thread(new RunImpl("/home/lildurden/Documents/these/suite/full/full/45107803-2017.json"));
	
	// Thread t1 = new Thread(new RunImpl("/home/lildurden/Documents/these/suite/full/full/45107803-2017.json"));
	// Thread t2 = new Thread(new RunImpl("/home/lildurden/Documents/these/suite/full/full/45107820-2017.json"));
	// Thread t3 = new Thread(new RunImpl("/home/lildurden/Documents/these/suite/full/full/45109400-2017.json"));
	// Thread t4 = new Thread(new RunImpl("/home/lildurden/Documents/these/suite/full/full/45109508-2017.json"));
	// Thread t5 = new Thread(new RunImpl("/home/lildurden/Documents/these/suite/full/full/45109545-2017.json"));
	// Thread t6 = new Thread(new RunImpl("/home/lildurden/Documents/these/suite/full/full/45109550-2017.json"));
	// Thread t7 = new Thread(new RunImpl("/home/lildurden/Documents/these/suite/full/full/45109619-2017.json"));
	// Thread t8 = new Thread(new RunImpl("/home/lildurden/Documents/these/suite/full/full/45109767-2017.json"));
	// Thread t9 = new Thread(new RunImpl("/home/lildurden/Documents/these/suite/full/full/45109775-2017.json"));

				
	// Thread t1 = new Thread(new RunImpl("/home/lildurden/Documents/these/suite/1dsort.json_1sort"));
	// Thread t2 = new Thread(new RunImpl("/home/lildurden/Documents/these/suite/2dsort.json_2sort"));
	// Thread t3 = new Thread(new RunImpl("/home/lildurden/Documents/these/suite/3dsort.json_3sort"));
	// Thread t4 = new Thread(new RunImpl("/home/lildurden/Documents/these/suite/4dsort.json_4sort"));
	// Thread t5 = new Thread(new RunImpl("/home/lildurden/Documents/these/suite/5dsort.json_5sort"));

	//t0.start();
	// t1.start();
	// t2.start();
	// t3.start();
	// t4.start();
	// t5.start();
	// t6.start();
	// t7.start();
	// t8.start();
	// t9.start();
	// t1.start();
	// t2.start();
	// t3.start();
	// t4.start();
	// t5.start();
	//readFile();
    }
    private void LoadRules(){
        ArrayList<Rule> rules=
    	    new ArrayList<Rule>();
    	rules=RuleManager.getDDBRules();
	if(rules!=null){
	    for(int i=0;i<rules.size();++i) {
		roleEventHandler.addRule(rules.get(i));
	    }
	}
    }    

    private String defineRule(HashMap<?, ?> sourceMap){
	String ret="";
	Rule rule=RuleManager.setRule(sourceMap);
	ArrayList<String> res_rule=new ArrayList<String>();
	if(rule.getForceWrite()){
	    res_rule=roleEventHandler.addRuleForced(rule);
	    
	}else{
	    res_rule=roleEventHandler.addRule(rule);	    
	}
	if(res_rule.size()==0){
	    RuleManager.writeMongo(rule);
	    ret="Rule processed";
	}else{
	    for (int i = 0; i < res_rule.size(); i++) {
		ret+=res_rule.get(i)+"\n";
	    }
	}
	return ret;
    }

    private void readFile(){

	String fileName = "/home/lildurden/Documents/these/Apt1-2017-full.json/Apt1-2017-full.json";
	List<String> list = new ArrayList<>();

	try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName))) {

	    //br returns as stream and convert it into a List
	    list = br.lines().collect(Collectors.toList());

	} catch (IOException e) {
	    e.printStackTrace();
	}

	ListIterator<String> it = list.listIterator();
	while(it.hasNext()){
	    String str = it.next();
	    //  System.out.println(str);
	    setPut(str);
	}
	
    }
    
    
    /**
     * Dispatch orders on Put method
     */
    private String setPut(String body){
	String ret = body;
        //LOG.info(body);
	if(!body.contains("<fail:")&&!body.equals("empty body")){
	    
	    HashMap<?, ?> bodyJson = Decode.getJsonRequest(body);
	    HashMap<?, ?> sourceMap = (bodyJson.get("_source")!=null?(HashMap<?, ?>)bodyJson.get("_source"):(HashMap<?, ?>)bodyJson);
	    //LOG.debug(sourceMap.toString());
	    //(HashMap<?, ?>)bodyJson;//.get("_source");
	    if(sourceMap.get("type")!=null){
		if(sourceMap.get("type").toString().equals("rule")){
		    return defineRule(sourceMap);
		}else{
		    String id=Decode.getIdFromSource(body);
		    createDispatchRole(id);
		    ret=dispatchRoles.get(id).dispatchPut(body);
		}
	    }else{
		//System.out.println(body);
		String id=Decode.getIdFromSource(body);
		//System.out.println(id);

		if(!id.equals("")){
		    createDispatchRole(id);
		    ret=dispatchRoles.get(id).dispatchPut(body);
		}else
		    ret="no id defined";
	    }
	}
	return ret;
	
    }
   
    private void setPost(String id){
	createDispatchRole(id);
    }
    private void createDispatchRole(String id){
	if(!dispatchRoles.containsKey(id)){
	    dispatchRoles.put(id,new DispatchRole(roleEventHandler,id,docal));
	    LOG.info("New user "+id);
	}	    
    }

    public class RunImpl implements Runnable {

	private String file;

	public RunImpl(String file){
	    this.file=file;
	}
	private String setPut(String body){
	    String ret = body;
	    //LOG.info(body);
	    if(!body.contains("<fail:")&&!body.equals("empty body")){
		HashMap<?, ?> bodyJson = Decode.getJsonRequest(body);
		HashMap<?, ?> sourceMap = (bodyJson.get("_source")!=null?(HashMap<?, ?>)bodyJson.get("_source"):(HashMap<?, ?>)bodyJson);
		//LOG.debug(sourceMap.toString());
		//(HashMap<?, ?>)bodyJson;//.get("_source");

		
		
		
		if(sourceMap.get("type")!=null){
		    if(sourceMap.get("type").toString().equals("rule")){
			return defineRule(sourceMap);
		    }else{
			String id=Decode.getIdFromSource(body);
			createDispatchRole(id);
			ret=dispatchRoles.get(id).dispatchPut(body);
		    }
		}else{
		    //System.out.println(body);
		    String id=Decode.getIdFromSource(body);
		    //System.out.println(id);

		    if(!id.equals("")){
			createDispatchRole(id);
			ret=dispatchRoles.get(id).dispatchPut(body);
		    }else
			ret="no id defined";
		}
	    }
	    return ret;
	
	}
	public void run() {
	    System.out.println(file);
	    long lasttime=0;
	    try{
	    
		File f = new File(file);

		BufferedReader b = new BufferedReader(new FileReader(f));

		String readLine = "";
	    
		while ((readLine = b.readLine()) != null) {
		    //System.out.println(readLine);
		    
		    boolean first=true;
		    HashMap<?, ?> bodyJson = Decode.getJsonRequest(readLine);
		    String sourceMap = bodyJson.get("@timestamp").toString();
		    long timestamp=stringToTimestamp(sourceMap);
		    long tttime=0;
		    if(lasttime==0){
		    	tttime=timestamp;
		    }else{
		    	tttime=((timestamp>lasttime)?timestamp-lasttime:0);
		    	if(first){
		    	    //System.out.println(tttime/1000/10+" "+timestamp+" "+lasttime);
		    	    try{
		    		Thread.sleep(tttime/1000/1000);
		    	    }catch (Exception ex){
		    		System.out.println("Error reading text message:" + ex);
		    	    }
		    	}else{
		    	    first=false;
		    	}
		    }
		    lasttime=timestamp;
		    //System.out.println(str);
		    setPut(readLine);
		}

	    
		// List<String> list = new ArrayList<>();
		// try (BufferedReader br = Files.newBufferedReader(Paths.get(file))) {
		// 	list = br.lines().collect(Collectors.toList());
	    } catch (IOException e) {
		e.printStackTrace();
	    }

	    // ListIterator<String> it = list.listIterator();

	    // long lasttime=0;

	    // //{"@timestamp":"2017-06-16T16:43:11.993Z","vera_serial":"45109508","eventTime":"2017-06-16T16:43:11.293Z","@version":"1","client_ip":"90.30.108.133","source":"vera","type":"event","event":{"variable":"Tripped","old_value":"1","device":14,"value":"0"},"device":{"name":"ContactS_Fridge","index":14,"id":"6","tripped":"0","room":"Kitchen"},"user":"DomassistDevillards"}
	    
	    
	    // while(it.hasNext()){
	    // 	String str = it.next();
	    // 	it.remove();
	    // 	boolean first=true;
	    // 	HashMap<?, ?> bodyJson = Decode.getJsonRequest(str);
	    // 	String sourceMap = bodyJson.get("@timestamp").toString();
	    // 	long timestamp=stringToTimestamp(sourceMap);
	    // 	long tttime=0;
	    // 	if(lasttime==0){
	    // 	    tttime=timestamp;
	    // 	}else{
	    // 	    tttime=((timestamp>lasttime)?timestamp-lasttime:0);
	    // 	    if(first){
	    // 		//System.out.println(tttime+" "+timestamp+" "+lasttime);
	    // 		try{
	    // 		    Thread.sleep(tttime/1000/100);
	    // 		}catch (Exception ex){
	    // 		    System.out.println("Error reading text message:" + ex);
	    // 		}
	    // 	    }else{
	    // 		first=false;
	    // 	    }
	    // 	}
	    // 	lasttime=timestamp;
	    // 	//System.out.println(str);
	    // 	setPut(str);
	    // }
	}
	private long stringToTimestamp(String date){
	    try{
		//System.out.println(date);
		SimpleDateFormat formatter =
		    new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
		Date fromGmt=formatter.parse(date);
		//System.out.println(formatter.format(fromGmt));

		SimpleDateFormat formatter2 =
		    new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
		//Date timezone = formatter.parse("2012-04-14 14:23:34");
		formatter2.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
		//System.out.println(formatter2.format(fromGmt));

	    
		Date lFromDate1 = formatter2.parse(formatter2.format(fromGmt));
		Timestamp fromTS1 = new Timestamp(lFromDate1.getTime());  
		return fromTS1.getTime();
	    }catch (Exception ex){
		System.out.println("Error reading text message:" + ex);
		return 0;
	    }
	}
    }
}
