package com.lildurden.softsens.cep.http;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.MalformedURLException; 
import java.io.IOException; 
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class SendHttpRequest {
    private static Logger LOG = LoggerFactory.getLogger(SendHttpRequest.class);

    private final String USER_AGENT = "Mozilla/5.0";

    private String url="http://localhost";
    private String port="8080";
    private URL objUrl;

    public SendHttpRequest(){
    	String finalUrl=url+(port.equals("")?"":":"+port);
    	try{
    	    objUrl = new URL(finalUrl);
    	}catch(MalformedURLException e){
    	    LOG.error("URL error "+e);
    	}	   
    }
    public SendHttpRequest(String url,String port,String url_args){
	this.url=url;
	this.port=port;
    	String finalUrl=url+(port.equals("")?"":":"+port);
	finalUrl+="/"+url_args;
    	try{
    	    objUrl = new URL(finalUrl);
    	}catch(MalformedURLException e){
    	    LOG.error("URL error "+e);
    	}	   
    }

    public String sendPost(String message){
	// String finalUrl=url;
	// objUrl = new URL(finalUrl);
	String ret="";
	try{
	    HttpURLConnection con = (HttpURLConnection) objUrl.openConnection();
	    con.setRequestMethod("POST");
	    con.setRequestProperty("User-Agent", USER_AGENT);
	    con.setDoOutput(true);
	    DataOutputStream wr = new DataOutputStream(con.getOutputStream());
	    wr.writeBytes(message);
	    wr.flush();
	    wr.close();
	
	    int responseCode = con.getResponseCode();

	    BufferedReader in = new BufferedReader(
						   new InputStreamReader(con.getInputStream()));
	    String inputLine;
	    StringBuffer response = new StringBuffer();

	    while ((inputLine = in.readLine()) != null) {
		response.append(inputLine);
	    }
	    in.close();
	    wr.close();
	    //System.out.println(response.toString());
	    ret=response.toString();
	}catch(IOException ie){
	    LOG.error("Request error "+ie);
	}
	//print result
	return ret;
    }
    
    public String sendPut(String message){
	// String finalUrl=url;
	// objUrl = new URL(finalUrl);
	String ret="";
	try{
	    HttpURLConnection con = (HttpURLConnection) objUrl.openConnection();
	    con.setRequestMethod("PUT");
	    con.setRequestProperty("User-Agent", USER_AGENT);
	    con.setDoOutput(true);
	    DataOutputStream wr = new DataOutputStream(con.getOutputStream());
	    wr.writeBytes(message);
	    wr.flush();
	    wr.close();
	
	    int responseCode = con.getResponseCode();

	    BufferedReader in = new BufferedReader(
						   new InputStreamReader(con.getInputStream()));
	    String inputLine;
	    StringBuffer response = new StringBuffer();

	    while ((inputLine = in.readLine()) != null) {
		response.append(inputLine);
	    }
	    in.close();
	    wr.close();
	    //System.out.println(response.toString());
	    ret=response.toString();
	}catch(IOException ie){
	    LOG.error("Request error "+ie);
	}
	//print result
	return ret;
    }
}
