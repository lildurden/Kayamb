package com.lildurden.softsens.cep.http;

import java.io.IOException;
import java.util.Map;
import java.util.HashMap;
import java.io.BufferedReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.http.HttpServletRequest;
import java.lang.StringBuilder;

import com.lildurden.softsens.cep.util.json.JSONUtils;

public class Decode{
    private static Logger LOG = LoggerFactory.getLogger(Decode.class);

    public static String getBody( HttpServletRequest request){
	try{
	    BufferedReader reader=request.getReader();
	    StringBuilder builder =  new StringBuilder();;
	    String body = "";
	    String aux = "";
	    while ((aux = reader.readLine()) != null) {
		builder.append(aux);
		body= builder.toString();
	    }
	    if(!body.isEmpty()){
		return body ;
	    }
	    return "empty body";
	}catch(IOException e){
	    LOG.error("read body error "+e);
	    return "<fail:readbody> "+e;
	}
    }
    public static String getIdFromSource(String body){
	String source="";
	if(!body.contains("<fail:")&&!body.equals("empty body")){
	    HashMap<?, ?> bodyJson = getJsonRequest(body);
	    HashMap<String, ?> bodyMap=
		(bodyJson.get("_source")!=null?(HashMap<String, ?>)bodyJson.get("_source"):(HashMap<String, ?>)bodyJson);	    
	    return bodyMap!=null?defineId(bodyMap):"empty body";
	}
	return body;
    }
    public static String defineId(HashMap<?, ?> bodyMap){
	String source=(bodyMap.get("source")!=null?bodyMap.get("source").toString():"");
	String id="";
	switch(source){
	case "vera":
	    String veraId="";
	    String type=(bodyMap.get("type")!=null?bodyMap.get("type").toString():"");
	    //if(type.equals("event"))
		veraId=(bodyMap.get("user")!=null?bodyMap.get("user").toString():bodyMap.get("vera_serial").toString());
		//else
		//veraId=bodyMap.get("vera_serial").toString();
	    id+=veraId;
	    break;
	case "tablet":
	    //break;
	case "userbox":
	    String user=bodyMap.get("user")!=null?bodyMap.get("user").toString():"";
	    id+=user;
	    break;
	case "rule":
	    id+=bodyMap.get("user").toString();
	    break;
	case "domassist":
	case "mongodb":
	case "diasuitebox":
	    id=source;
	    break;
	case "":
	    user=bodyMap.get("user").toString();
	    id+=user;
	default:
	    id=source;
	    break;
	}
	return id;
    }
    /**
     * get string as json
     * duplicated in DispatchRole
     */
    public static HashMap<?, ?> getJsonRequest(String text){
	Object jsonData = new HashMap<Object, Object>();
	if(!text.equals("")){
	    //System.out.println(text);
	jsonData=JSONUtils.decodeJSON(text);
	if(!((HashMap<?, ?>)jsonData).isEmpty()){
	    return (HashMap<?, ?>)jsonData;
	}
	}
	return null;		
    }
}
