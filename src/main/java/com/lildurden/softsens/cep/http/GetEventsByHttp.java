package com.lildurden.softsens.cep.http;

import java.lang.InterruptedException ;

//import com.lildurden.softsens.cep.util.role.DispatchRole;
import com.lildurden.softsens.cep.handler.RoleEventHandler;

import java.util.Map;
import java.util.Iterator;
import java.util.HashMap;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.eclipse.jetty.server.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GetEventsByHttp{
    private static Logger LOG = LoggerFactory.getLogger(GetEventsByHttp.class);

    private RoleEventHandler roleEventHandler;
    
    public GetEventsByHttp(){
	roleEventHandler=new RoleEventHandler();
	LOG.info("getEventHttp");
    }
    public void startServer(){
	try{
	    Server server = new Server(8080);
	
	    server.setHandler(new HandlerHttp(roleEventHandler));
	
	    server.start();
		
	    server.join();
	    server.destroy();
	}catch(InterruptedException e){
	    LOG.error("Server launch error "+e);
	}catch(Exception e){
	    LOG.error("Request launch "+e);
	}
    }
}
