package com.lildurden.softsens.cep.http;

import com.lildurden.softsens.cep.event.manager.DispatchRole;
import com.lildurden.softsens.cep.handler.RoleEventHandler;
import com.lildurden.softsens.cep.subscriber.manager.RuleManager;
import com.lildurden.softsens.cep.subscriber.manager.Rule;
import com.lildurden.softsens.cep.util.inout.Output;
import com.lildurden.softsens.cep.util.inout.MongoWrite;
import com.lildurden.softsens.cep.util.DoCalendar;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.jetty.server.handler.AbstractHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.io.BufferedReader;

public class HandlerHttp extends AbstractHandler {
    private static Logger LOG = LoggerFactory.getLogger(HandlerHttp.class);
    
    // private HttpServletRequest request;
    // private HttpServletResponse response;
    private Map<String,DispatchRole> dispatchRoles;
            
    private RoleEventHandler roleEventHandler;

    private DoCalendar docal=new DoCalendar();
    
    public HandlerHttp(RoleEventHandler roleEventHandler){
	this.roleEventHandler=roleEventHandler;
	dispatchRoles=new HashMap<String,DispatchRole>();
	LoadRules();
    }
    public HandlerHttp(){
	dispatchRoles=new HashMap<String,DispatchRole>();
	LoadRules();
    }
    private void LoadRules(){
        ArrayList<Rule> rules=
    	    new ArrayList<Rule>();
    	rules=RuleManager.getDDBRules();
	if(rules!=null){
	    for(int i=0;i<rules.size();++i) {
		roleEventHandler.addRule(rules.get(i));
	    }
	}
    }    
    /**
     * Write the response from the request
     */
    // private void response(String message){
    // 	try{
    // 	    response.setContentType("text/html; charset=utf-8");
    // 	    response.setStatus(HttpServletResponse.SC_OK);
    // 	    response.getWriter().println("<h1>"+message+"</h1>");
    // 	}catch(IOException e){
    // 	    LOG.error("response error "+e);
    // 	}
    // }

    private String defineRule(HashMap<?, ?> sourceMap){
	String ret="";
	Rule rule=RuleManager.setRule(sourceMap);
	ArrayList<String> res_rule=new ArrayList<String>();
	if(rule.getForceWrite()){
	    res_rule=roleEventHandler.addRuleForced(rule);
	    
	}else{
	    res_rule=roleEventHandler.addRule(rule);	    
	}
	if(res_rule.size()==0){
	    RuleManager.writeMongo(rule);
	    ret="Rule processed";
	}else{
	    for (int i = 0; i < res_rule.size(); i++) {
		ret+=res_rule.get(i)+"\n";
	    }
	}
	return ret;
    }
    
    /**
     * Dispatch orders on Put method
     */
    private String setPut(String body){
	String ret = body;
        //LOG.info(body);
	if(!body.contains("<fail:")&&!body.equals("empty body")){
	    
	    HashMap<?, ?> bodyJson = Decode.getJsonRequest(body);
	    HashMap<?, ?> sourceMap = (bodyJson.get("_source")!=null?(HashMap<?, ?>)bodyJson.get("_source"):(HashMap<?, ?>)bodyJson);
	    //LOG.debug(sourceMap.toString());
	    //(HashMap<?, ?>)bodyJson;//.get("_source");
	    if(sourceMap.get("type")!=null){
		if(sourceMap.get("type").toString().equals("rule")){
		    return defineRule(sourceMap);
		}else{
		    String id=Decode.getIdFromSource(body);
		    createDispatchRole(id);
		    ret=dispatchRoles.get(id).dispatchPut(body);
		}
	    }else{
		//System.out.println(body);
		String id=Decode.getIdFromSource(body);
		//System.out.println(id);

		if(!id.equals("")){
		    createDispatchRole(id);
		    ret=dispatchRoles.get(id).dispatchPut(body);
		}else
		    ret="no id defined";
	    }
	}
	return ret;
	
    }
    private String setDelete(String path){
	String message="Rule deleted";
        String path_[]=getPath(path);
	if(path_.length>1){
	    if(path_[0].equals("rule")){
		int ret=roleEventHandler.destroyRule(path_[1]);
		switch(ret){
		case 0: MongoWrite.getInstance().deleteRule(path_[1]);
		    break;
		case -1: message="error DELETE -1: often one part of the rule was not correctly set in a first time, should be deleted";
		    break;
		case -2:
		    message="error DELETE -2: the rule does not exists";
		    break;
		default:
		    break;
		};
	    }
	}
	return message;
    }
    private void setPost(String id){
	createDispatchRole(id);
    }
    private void createDispatchRole(String id){
	if(!dispatchRoles.containsKey(id)){
	    dispatchRoles.put(id,new DispatchRole(roleEventHandler,id,docal));
	    LOG.info("New user "+id);
	}	    
    }
    
    /**
     * Dispatch order depending on http method
     */
    private String dispatch(String body,String method,String path){
	//String id="";
	//String order=path[0];
	String returnString="unkown operation, nothing to do";
	//body=Decode.getBody(request);
	// if(!body.contains("<fail:")&&!body.equals("empty body")){
	//     id=Decode.getIdFromSource(body);
	// }
	// useless if all data provided following logstash format
 	//id=(id.equals("shit")?path[1]:id);
	switch(method){
	case "GET" :
	    break;
	case "POST" :
	    //setPost(id);
	    break;
	case "DELETE" :
	    returnString=setDelete(path);
	    break;
	case "PUT" :
	    returnString=setPut(body);
	    break;
	default:
	    break;
	}
	return returnString;
    }
    /**
     * Extract path of the request
     */
    private String[] getPath(String target){
    	if(target.charAt(0)=='/')
    	    target=target.substring(1);
    	return target.split("/");
    }
    /**
     * Handle the request
     */
    public void handle( String target,
                        Request baseRequest,
                        HttpServletRequest request,
                        HttpServletResponse response){
	try{
	    //this.request=request;

	    //this.response=response;
	    String method=request.getMethod();
	    
	    String body=Decode.getBody(request);
	    
	    String path=baseRequest.getPathInfo();
	    //LOG.debug("body: " +body);
	    //Output.write("/opt/webapp/bodycount",body+'\n');

	    //String res=;
	    //response("lolilol");

	    response.setContentType("text/html; charset=utf-8");
	    response.setStatus(HttpServletResponse.SC_OK);
	    response.getWriter().println(dispatch(body,method,path));
	    
	    // Inform jetty that this request has now been handled
	    baseRequest.setHandled(true);
	    
	    
	}catch(IOException ioe){
	    LOG.error("ioError "+ioe);
	}
	    // }catch(ServletException serve){
	//     LOG.error("servletError "+serve);
	// }
    }

    // private String HeadersToString(){
    // 	StringBuilder sb = new StringBuilder();
    //     Iterator<Map.Entry<String, String>> iter = getHeadersInfo().entrySet().iterator();
    //     while (iter.hasNext()) {
    // 	    Map.Entry<String, String> entry = iter.next();
    //         sb.append(entry.getKey());
    //         sb.append('=').append('"');
    //         sb.append(entry.getValue());
    //         sb.append('"');
    //         if (iter.hasNext()) {
    //             sb.append(',').append(' ');
    //         }
    //     }
    //     return sb.toString();
    // }   
    // private Map<String, String> getHeadersInfo() {

    // 	Map<String, String> map = new HashMap<String, String>();

    // 	Enumeration headerNames = request.getHeaderNames();
    // 	while (headerNames.hasMoreElements()) {
    // 	    String key = (String) headerNames.nextElement();
    // 	    String value = request.getHeader(key);
    // 	    map.put(key, value);
    // 	}
    // 	return map;
    // }  
}
