package com.lildurden.softsens.cep.util.inout;

import java.io.BufferedWriter;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
public final class Files{
    private static final Files inst= new Files();

    private Files(){
	super();
    }

    public static Files getInstance() {
        return inst;
    }
    
    public synchronized boolean exists(String pathFile){
        File file = new File(pathFile);
	return file.exists();
    }

    public synchronized void write(String path,String message){
	write(path,message,false);
    }
    public synchronized void writeNext(String path,String message){
	write(path,message,true);
    }
    
    private void write(String path,String message,boolean append){
	try{
	    File file = new File(path);
	    if (!file.exists()) {
		file.getParentFile().mkdirs();
		file.createNewFile();
	    }
	    FileWriter writer = new FileWriter(file.getAbsoluteFile(),append);
	    BufferedWriter bw = new BufferedWriter(writer);
	    PrintWriter out = new PrintWriter(bw);
	    out.println(message);
	    bw.close();
	}catch(IOException e){
	    System.out.println("cant write "+e);
	}
    }
    
    public synchronized List<String> read(String path){
	List<String> records = new ArrayList<String>();
	try{
	    BufferedReader reader = new BufferedReader(new FileReader(path));
	    String line;
	    while ((line = reader.readLine()) != null){
		if(!line.equals(""))
		    records.add(line);
	    }
	    reader.close();
	    return records;
	}catch(Exception e){
	    System.out.println("cant read "+e);
	}
	return null;
    }

    public synchronized List<String> getFolderContents(String path){
	List<String> directoriesNames = new ArrayList<String>();
	File folder = new File(path);
	File[] listOfFiles = folder.listFiles();
	//System.out.println(path);
	if(listOfFiles!=null){
	    for (int i = 0; i < listOfFiles.length; i++) {
		// if (listOfFiles[i].isFile()) {
		// 	System.out.println("File " + listOfFiles[i].getName());
		// } else if (listOfFiles[i].isDirectory()) {
		if(!listOfFiles[i].getName().equals(".git") &&
		   !listOfFiles[i].getName().equals("oldies") &&
		   !listOfFiles[i].getName().equals("testing") &&
		   !listOfFiles[i].getName().equals("README.md") /* &&
		   !listOfFiles[i].getName().equals("operators")*/){
		    //System.out.println("LOOOL "+listOfFiles[i].getName());
		    directoriesNames.add(listOfFiles[i].getName());
		}
		 // if(listOfFiles[i].getName().equals("testing")){
		 // 	directoriesNames.add(listOfFiles[i].getName());
		 //    }  
	    }
	}
	return directoriesNames;
    }
    public synchronized List<String> getFolderContents(String path,boolean test){
	List<String> directoriesNames = new ArrayList<String>();
	File folder = new File(path);
	File[] listOfFiles = folder.listFiles();
	//System.out.println(path);
	if(listOfFiles!=null){
	    for (int i = 0; i < listOfFiles.length; i++) {
		// if (listOfFiles[i].isFile()) {
		// 	System.out.println("File " + listOfFiles[i].getName());
		// } else if (listOfFiles[i].isDirectory()) {
		if(test){
		    if(listOfFiles[i].getName().equals("testing")){
			directoriesNames.add(listOfFiles[i].getName());
		    }  
		}else{
		    if(!listOfFiles[i].getName().equals(".git") &&
		       !listOfFiles[i].getName().equals("oldies") &&
		       !listOfFiles[i].getName().equals("testing") &&
		       !listOfFiles[i].getName().equals("README.md") /* &&
									!listOfFiles[i].getName().equals("operators")*/){
			//System.out.println("LOOOL "+listOfFiles[i].getName());
			directoriesNames.add(listOfFiles[i].getName());
		    }
		}
	    }
	}
	return directoriesNames;
    }
}
