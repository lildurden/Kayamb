package com.lildurden.softsens.cep.util;


public class Chrono{
    private long begin;
    private long end;
    private long pause;
    private long restart=0;
    private long interm=0;
    private boolean ispause=false;
    public Chrono(){}
    public void start(){
	System.out.println("Start chrono ");
	begin = System.currentTimeMillis();
    }
    public void pause(){
	System.out.println("Pause chrono");
	if(!ispause){
	pause=System.currentTimeMillis();
	if(interm==0)
	    interm=pause-begin;
	else
	    interm+=pause-restart;
	ispause=true;
	}
    }
    public void restart(){
	if(ispause){
	    System.out.println("Restart chrono ");
	    restart=System.currentTimeMillis();
	    ispause=false;
	}
    }
	
    public void stop(){
	end = System.currentTimeMillis();
	System.out.println("End chrono "+begin+" "+end);
    }

    public long getTime(){
	if(interm==0)
	    return end-begin;
	else
	    return interm+(end-restart);
    }
    
}
