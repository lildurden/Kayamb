package com.lildurden.softsens.cep.util.data;

import java.util.Date;


//Unused
public class Log{
    public String location;
    public String type;
    public String status;
    public Date date;
    //public long last;
    public Log(String location,String type,String status,Date date){
	this.location=location;
	this.type=type;
	this.status=status;
	this.date=date;
    }
    public String getLocation(){return location;}
    public String getType(){return type;}
    public String getStatus(){return status;}
    public Date getDate(){return date;}
}
