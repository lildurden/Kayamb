package com.lildurden.softsens.cep.util.json;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.WildcardType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonString;
import javax.json.JsonValue;
import javax.json.JsonValue.ValueType;

import org.apache.commons.lang3.ArrayUtils;

/**
 * A utility class to decode/encode JSON strings
 */
public class JSONUtils {

    private static final String DATE_FIELD = "$date";
    public static final String CLASS_KEY = "1class";
    public static final String VALUE_KEY = "value";

    public static final String FILE_PREFIX = "#__file__#:";

    static public class IncompatibleTypeException extends Exception {
        private static final long serialVersionUID = 1L;

        public IncompatibleTypeException() {
            super("Incompatible type in object conversion");
        }

        public IncompatibleTypeException(Throwable e) {
            super("Incompatible type in object conversion", e);
        }

        public IncompatibleTypeException(String string) {
            super(string);
        }

        public IncompatibleTypeException(Class<?> class1, Class<?> clazz) {
            super("Object of class " + class1.toString() + " cannot be converted to " + clazz.toString());
        }
    }

    /**
     * Encode an object in JSON
     * 
     * @param object
     *            The object to convert to JSON
     * @return a string with the JSON encoding with potential references to files
     * @throws IOException
     */
    static public String encodeJSON(Object object) throws IOException {
        Object json = toJSON(object);
        if (json == null)
            return "";
        if (json instanceof String) {
            return "\""
                    + ((String) json).replace("\\", "\\\\").replace("\n", "\\n").replace("\r", "\\r")
                            .replace("'", "\\'") + "\"";
        }
        return json.toString();
    }

    public static boolean isPrimitive(Object o) {
        return o == null || isPrimitive(o.getClass());
    }

    public static boolean isPrimitive(Class<?> clazz) {
        return clazz.isPrimitive() || clazz.equals(String.class) || clazz.equals(Integer.class)
                || clazz.equals(Boolean.class) || clazz.equals(Short.class) || clazz.equals(Byte.class)
                || clazz.equals(Long.class) || clazz.equals(Float.class) || clazz.equals(Double.class)
                || clazz.equals(Void.class);
    }

    public static boolean isAssignableFrom(Class<?> c1, Class<?> c2) {
        return c1.isAssignableFrom(c2) || isBoolean(c1) && isBoolean(c2) || isInteger(c1) && isInteger(c2)
                || isFloat(c1) && (isFloat(c2) || isInteger(c2));
    }

    private static boolean isBoolean(Class<?> c) {
        return c.getName().equals("boolean") || c.equals(Boolean.class);
    }

    private static boolean isInteger(Class<?> c) {
        String name = c.getName();
        return name.equals("byte") || name.equals("int") || name.equals("long") || name.equals("short")
                || c.equals(Byte.class) || c.equals(Integer.class) || c.equals(Long.class) || c.equals(Short.class);
    }

    private static boolean isFloat(Class<?> c) {
        String name = c.getName();
        return name.equals("float") || name.equals("double") || c.equals(Float.class) || c.equals(Double.class);
    }

    private static Object[] toObjectArray(Object array) {
        if (array instanceof int[]) {
            return ArrayUtils.toObject((int[]) array);
        } else if (array instanceof float[]) {
            return ArrayUtils.toObject((float[]) array);
        } else if (array instanceof byte[]) {
            return ArrayUtils.toObject((byte[]) array);
        } else if (array instanceof long[]) {
            return ArrayUtils.toObject((long[]) array);
        } else if (array instanceof boolean[]) {
            return ArrayUtils.toObject((boolean[]) array);
        } else if (array instanceof short[]) {
            return ArrayUtils.toObject((short[]) array);
        } else if (array instanceof char[]) {
            return ArrayUtils.toObject((char[]) array);
        } else if (array instanceof double[]) {
            return ArrayUtils.toObject((double[]) array);
        }

        return (Object[]) array;

    }

    private static JsonArrayBuilder buildArray(Object array, JsonArrayBuilder arrayBuilder) throws IOException {
        Object[] o = toObjectArray(array);
        for (int i = 0; i < o.length; i++) {
            arrayBuilder.add(toJSON(o[i]));
        }
        return arrayBuilder;
    }

    private static JsonValue toJSON(Object object) throws IOException {
        if (isPrimitive(object))
            return toJsonValue(object);
        else if (object instanceof Date) {
            return Json.createObjectBuilder().add(DATE_FIELD, ((Date) object).getTime()).build();
        } else if (object.getClass().isArray()) {
            return buildArray(object, Json.createArrayBuilder()).build();
        } else if (object instanceof Map) {
            Map<?, ?> map = (Map<?, ?>) object;
            JsonObjectBuilder json = Json.createObjectBuilder();
            for (Map.Entry<?, ?> entries : map.entrySet()) {
                json.add(entries.getKey().toString(), toJSON(entries.getValue()));
            }
            return json.build();
        } else if (object.getClass().isEnum()) { // enumerations
            return toJsonValue(object.toString());
        } else if (object instanceof Collection) {
            JsonArrayBuilder array = Json.createArrayBuilder();
            Collection<?> l = (Collection<?>) object;
            for (Object o : l) {
                array.add(toJSON(o));
            }
            return array.build();
        } else if (object instanceof Serializable) { // structures defined in the language
            Class<?> c = object.getClass();
            JsonObjectBuilder jsonBuilder = Json.createObjectBuilder();
            for (Method m : c.getMethods()) {
                String name = m.getName();
                if (name.length() >= 4 && name.startsWith("get") && !name.equals("getClass")
                        && m.getParameterTypes().length == 0) {
                    try {
                        JsonValue value = toJSON(m.invoke(object));
                        jsonBuilder.add(name.substring(3, 4).toLowerCase() + name.substring(4), value);
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }
                }
            }
            return jsonBuilder.build();
        }
        return JsonValue.NULL;
    }

    private static JsonValue toJsonValue(Object object) {
        JsonObjectBuilder builder = Json.createObjectBuilder();
        if (object == null) {
            builder.addNull("object");
            return builder.build().get("object");
        }
        Class<?> clazz = object.getClass();
        if (clazz.isPrimitive()) {
            switch (clazz.getName().charAt(0)) {
            case 'Z': // boolean
                builder.add("object", (boolean) object);
                break;
            case 'B': // byte
                builder.add("object", (byte) object);
                break;
            case 'C': // char
                builder.add("object", (char) object);
                break;
            case 'D': // double
                builder.add("object", (double) object);
                break;
            case 'F': // float
                builder.add("object", (float) object);
                break;
            case 'I': // int
                builder.add("object", (int) object);
                break;
            case 'J': // long
                builder.add("object", (long) object);
                break;
            case 'S': // short
                builder.add("object", (short) object);
                break;
            default:
                builder.addNull("object");
                break;
            }
        } else if (clazz.equals(String.class)) {
            builder.add("object", (String) object);
        } else if (clazz.equals(Integer.class)) {
            builder.add("object", (Integer) object);
        } else if (clazz.equals(Boolean.class)) {
            builder.add("object", (Boolean) object);
        } else if (clazz.equals(Long.class)) {
            builder.add("object", (Long) object);
        } else if (clazz.equals(Short.class)) {
            builder.add("object", (Short) object);
        } else if (clazz.equals(Byte.class)) {
            builder.add("object", (Byte) object);
        } else if (clazz.equals(Float.class)) {
            builder.add("object", (Float) object);
        } else if (clazz.equals(Double.class)) {
            builder.add("object", (Double) object);
        } else if (clazz.equals(Void.class)) {
            builder.addNull("object");
        }
        return builder.build().get("object");
    }

    /**
     * Take a JSON string and convert it into a classical objects (List, Maps, Strings and byte[])
     * 
     * @param jsonString
     *            the string to decode
     * @return
     */
    static public Object decodeJSON(String jsonString) {
        if (jsonString == null)
            return null;
        jsonString = jsonString.trim();
        if (jsonString.isEmpty() || jsonString.equals("null"))
            return null;
        char c = jsonString.charAt(0);
        if (c != '{' && c != '[') {
            // primitive types
            if (c == '\'' || c == '\"') {
                // String
                char[] cs = new char[1];
                cs[0] = c;
                String s = new String(cs);
                if (!jsonString.endsWith(s))
                    return null;
                jsonString = jsonString.substring(1, jsonString.length() - 1);
                return jsonString.replaceAll("\\\\([^nr])", "$1").replace("\\n", "\n").replace("\\r", "\r");
            } else if (jsonString.equals("true") || jsonString.equals("false")) {
                return Boolean.valueOf(jsonString);
            } else if (jsonString.matches("^[+-]?[0-9]+$")) {
                try {
                    return Integer.valueOf(jsonString);
                } catch (NumberFormatException e) {
                    return Long.valueOf(jsonString);
                }
            } else if (jsonString.matches("^[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?$")) {
                return Double.valueOf(jsonString);
            } else
                return null;
        }
        return getObjectFromJSON(toJSON(jsonString));
    }

    private static JsonValue toJSON(String jsonString) {
        JsonReader reader = Json.createReader(new StringReader(jsonString));
        return reader.read();
    }

    /**
     * Decode an object that came out from JSON into the clazz object, throws an exception
     * 
     * @param jsonDecoded
     *            the object that was decoded
     * @param type
     *            The type to convert to
     * @return an object of type clazz converted from jsonDecoded
     * @throws IncompatibleTypeException
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    static public Object decode(Object jsonDecoded, Type type) throws IncompatibleTypeException {
        Class<?> clazz = null;
        if (type == null || type instanceof WildcardType)
            return jsonDecoded;

        if (type instanceof Class<?>)
            clazz = ((Class<?>) type);

        if (jsonDecoded == null)
            return null;
        if (type instanceof ParameterizedType) {
            clazz = (Class<?>) ((ParameterizedType) type).getRawType();
        }

        if (List.class.isAssignableFrom(clazz)) {
            List<Object> list = new ArrayList<Object>();
            Type componentType = Object.class;
            if (type != null && type instanceof ParameterizedType) {
                componentType = ((ParameterizedType) type).getActualTypeArguments()[0];
            }
            if (jsonDecoded.getClass().isArray()) {
                int length = Array.getLength(jsonDecoded);
                for (int i = 0; i < length; i++)
                    list.add(decode(Array.get(jsonDecoded, i), componentType));
                return list;
            } else if (jsonDecoded instanceof List) {
                List<?> l = (List<?>) jsonDecoded;
                for (Object o : l)
                    list.add(decode(o, componentType));
                return list;
            }
            throw new IncompatibleTypeException(jsonDecoded.getClass(), clazz);
        } else if (Map.class.isAssignableFrom(clazz)) {
            if (!(jsonDecoded instanceof Map))
                throw new IncompatibleTypeException(jsonDecoded.getClass(), clazz);
            if (!(type instanceof ParameterizedType))
                return jsonDecoded;
            Type keyType = ((ParameterizedType) type).getActualTypeArguments()[0];
            Type valueType = ((ParameterizedType) type).getActualTypeArguments()[1];
            Map result = new HashMap();
            Map map = (Map) jsonDecoded;
            for (Object key : map.keySet()) {
                result.put(decode(key, keyType), decode(map.get(key), valueType));
            }
            return result;
        } else if (type instanceof GenericArrayType || clazz.isArray()) {
            Type componentType;
            if (type instanceof GenericArrayType)
                componentType = ((GenericArrayType) type).getGenericComponentType();
            else
                componentType = clazz.getComponentType();
            if (clazz.getName().equals("[B") && jsonDecoded instanceof byte[]) // handle specially byte array
                return jsonDecoded;
            if (jsonDecoded.getClass().isArray()) {
                int length = Array.getLength(jsonDecoded);
                Object array = Array.newInstance((Class<?>) componentType, length);
                for (int i = 0; i < length; i++)
                    Array.set(array, i, decode(Array.get(jsonDecoded, i), componentType));
                return array;
            } else if (jsonDecoded instanceof List) {
                List<?> l = (List<?>) jsonDecoded;
                int length = l.size();
                Object array = Array.newInstance((Class<?>) componentType, length);
                for (int i = 0; i < length; i++)
                    Array.set(array, i, decode(l.get(i), componentType));
                return array;

            }
            throw new IncompatibleTypeException(jsonDecoded.getClass(), clazz);
        } else if (isPrimitive(clazz)) {
            if (isAssignableFrom(clazz, jsonDecoded.getClass()))
                return convert(jsonDecoded, clazz);
            throw new IncompatibleTypeException(jsonDecoded.getClass(), clazz);
        } else if (clazz.isEnum()) {
            if (!(jsonDecoded instanceof String)) {
                throw new IncompatibleTypeException();
            }
            for (Object e : clazz.getEnumConstants()) {
                if (e.toString().equals(jsonDecoded))
                    return e;
            }
            throw new IncompatibleTypeException();
        } else if (String.class.equals(clazz)) {
            if (jsonDecoded instanceof String)
                return jsonDecoded;
            throw new IncompatibleTypeException(jsonDecoded.getClass(), clazz);
        } else if (clazz.equals(Object.class)) {
            return jsonDecoded;
        } else if (Serializable.class.isAssignableFrom(clazz)) {
            if (clazz.equals(jsonDecoded.getClass()))
                return jsonDecoded;
            if (!(jsonDecoded instanceof Map))
                throw new IncompatibleTypeException(jsonDecoded.getClass(), clazz);
            try {
                Map<?, ?> map = (Map<?, ?>) jsonDecoded;
                Object result = clazz.newInstance();
                for (Method method : clazz.getMethods()) {
                    String name = method.getName();
                    Type[] types = method.getGenericParameterTypes();
                    // Calling setters

                    if (name.startsWith("set") && name.length() > 3 && types.length == 1) {
                        name = name.substring(3, 4).toLowerCase() + name.substring(4);
                        if (map.containsKey(name)) {
                            method.invoke(result, decode(map.get(name), types[0]));
                        }
                    }
                }
                return result;
            } catch (InstantiationException e) {
                throw new IncompatibleTypeException(e);
            } catch (IllegalAccessException e) {
                throw new IncompatibleTypeException(e);
            } catch (IllegalArgumentException e) {
                throw new IncompatibleTypeException(e);
            } catch (InvocationTargetException e) {
                throw new IncompatibleTypeException(e);
            }
        } else if (clazz.equals(jsonDecoded.getClass())) {
            return jsonDecoded;
        }
        throw new IncompatibleTypeException(jsonDecoded.getClass(), clazz);
    }

    private static Object getIntValue(long value, Class<?> clazz) {
        if (clazz.equals(Integer.class) || clazz.getName().equals("int"))
            return (int) value;
        if (clazz.equals(Byte.class) || clazz.getName().equals("byte"))
            return (byte) value;
        if (clazz.equals(Short.class) || clazz.getName().equals("short"))
            return (short) value;
        return value;
    }

    private static Object getFloatValue(double value, Class<?> clazz) {
        if (clazz.equals(Float.class) || clazz.getName().equals("float"))
            return (float) value;
        return value;
    }

    private static Object convert(Object jsonDecoded, Class<?> clazz) {
        if (clazz.isAssignableFrom(jsonDecoded.getClass()))
            return jsonDecoded;
        if (isBoolean(clazz))
            if (jsonDecoded instanceof Number)
                return ((Number) jsonDecoded).byteValue() != 0;
            else
                return Boolean.parseBoolean(jsonDecoded.toString());
        if (isFloat(clazz))
            if (jsonDecoded instanceof Number)
                return getFloatValue(((Number) jsonDecoded).doubleValue(), clazz);
            else
                return getFloatValue(Double.parseDouble(jsonDecoded.toString()), clazz);
        if (isInteger(clazz))
            if (jsonDecoded instanceof Number)
                return getIntValue(((Number) jsonDecoded).longValue(), clazz);
            else
                return getIntValue(Long.parseLong(jsonDecoded.toString()), clazz);
        return jsonDecoded;
    }

    private static Object convert(Object object) {
        Class<?> clazz = object.getClass();
        if (object instanceof Number)
            if (isFloat(clazz))
                return ((Number) object).floatValue();
        if (isInteger(clazz))
            if (object instanceof Number)
                return ((Number) object).intValue();
        return object;
    }

    static private Object getObjectFromJSON(JsonValue jsonObject) {
        if (jsonObject == null) {
            return jsonObject;
        } else {
            switch (jsonObject.getValueType()) {
            case OBJECT:
                JsonObject obj = (JsonObject) jsonObject;
                if (obj.containsKey(DATE_FIELD) && obj.size() == 1
                        && obj.get(DATE_FIELD).getValueType() == ValueType.NUMBER) {
                    return new Date(obj.getJsonNumber(DATE_FIELD).longValue());
                }
                return getMapFromJSON((JsonObject) jsonObject);
            case ARRAY:
                return getListFromJSON((JsonArray) jsonObject);
            case FALSE:
                return false;
            case NULL:
                return null;
            case NUMBER:
                JsonNumber number = (JsonNumber) jsonObject;
                return number.isIntegral() ? (Object) number.longValue() : (Object) number.doubleValue();
            case STRING:
                String s = ((JsonString) jsonObject).getString();
                return s;
            case TRUE:
                return true;
            }
        }
        return null;
    }

    static private Map<String, Object> getMapFromJSON(JsonObject object) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        if (object != null) {
            for (Object s : object.keySet()) {
                result.put(s.toString(), getObjectFromJSON(object.get(s)));
            }
        }
        return result;
    }

    private static List<Object> getListFromJSON(JsonArray jsonObject) {
        ArrayList<Object> r = new ArrayList<Object>();
        for (int i = 0; i < jsonObject.size(); i++)
            r.add(getObjectFromJSON(jsonObject.get(i)));
        return r;
    }

    /**
     * Convert a serializable object to a map by reflective accessors
     * 
     * @param object
     *            The serializable object to convert
     * @return a map that contains, for each method "getSomething()" in object, a key value pair with "something" as key
     *         and the return value of getSomething() as value
     */
    public static Map<String, Object> toMap(Serializable object) {
        Map<String, Object> output = new HashMap<String, Object>();
        Class<?> c = object.getClass();
        for (Method m : c.getMethods()) {
            String name = m.getName();
            if (name.length() >= 4 && name.startsWith("get") && !name.equals("getClass")
                    && m.getParameterTypes().length == 0) {
                try {
                    Object value = m.invoke(object);
                    output.put(name.substring(3, 4).toLowerCase() + name.substring(4), value);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
        return output;
    }

    /**
     * Recursively convert a serializable object to a Map<String,Object> by reflective accessors. This methods lost the
     * original type
     * 
     * @param object
     *            The serializable object to convert
     * @return a map that contains, for each method "getSomething()" in object, a key value pair with "something" as key
     *         and the return value of getSomething() as value
     */
    public static Object encode(Object object) {
        return encodeToMap(object, false);
    }

    /**
     * Recursively convert a serializable object to a Map<String,Object> by reflective accessors. This methods keeps the
     * original type if the object is a serializable class
     * 
     * @param object
     *            The serializable object to convert
     * @return a map that contains, for each method "getSomething()" in object, a key value pair with "something" as key
     *         and the return value of getSomething() as value
     */
    public static Object encodeToMap(Object object) {
        return encodeToMap(object, true);
    }

    /**
     * Recursively convert a serializable object to a Map<String,Object> by reflective accessors
     * 
     * @param object
     *            The serializable object to convert
     * @param embedType
     *            set it to true to embed the class name in the encoded map
     * @return a map that contains, for each method "getSomething()" in object, a key value pair with "something" as key
     *         and the return value of getSomething() as value
     */
    private static Object encodeToMap(Object object, boolean embedType) {
        if (isPrimitive(object))
            return object;
        else if (object instanceof Date) {
            // Map<String, Object> encoded = new HashMap<String, Object>();
            // encoded.put(CLASS_KEY, Date.class.getName());
            // encoded.put(VALUE_KEY, ((Date) object).getTime());
            return object;
        } else if (object.getClass().isArray()) {
            Object[] o = (Object[]) object;
            ArrayList<Object> array = new ArrayList<Object>();
            for (int i = 0; i < o.length; i++) {
                array.add(i, encodeToMap(o[i], embedType));
            }
            return array;
        } else if (object instanceof Map<?, ?>) { // This is a real attribute map
            @SuppressWarnings("unchecked")
            Map<String, Object> map = (Map<String, Object>) object;
            Map<String, Object> encoded = new HashMap<String, Object>();
            for (Map.Entry<String, Object> entries : map.entrySet()) {
                encoded.put(entries.getKey(), encodeToMap(entries.getValue(), embedType));
            }
            return encoded;
        } else if (object instanceof Collection) {
            Collection<Object> col = new LinkedList<Object>();
            for (Object o : (Collection<?>) object) {
                col.add(encodeToMap(o, embedType));
            }
            return col;
        } else if (object.getClass().isEnum()) {
            if (embedType) {
                Map<String, Object> encoded = new HashMap<String, Object>();
                encoded.put(CLASS_KEY, object.getClass().getName());
                encoded.put(VALUE_KEY, object.toString());
                return encoded;
            } else
                return object.toString();
        } else if (object instanceof Serializable) { // structures defined in the language
            Class<?> c = object.getClass();
            Map<String, Object> encoded = toMap((Serializable) object);
            if (embedType)
                encoded.put(CLASS_KEY, c.getName());
            for (String s : encoded.keySet()) {
                encoded.put(s, encodeToMap(encoded.get(s), embedType));
            }
            return encoded;
        }
        return null;
    }

    /**
     * Recursively decode an object from an encoded serializable Map<String,Object> object
     * 
     * @param object
     *            The encoded object
     * @return a well typed object
     */
    public static Object decodeFromMap(Object object) {
        if (object == null)
            return null;
        else if (isPrimitive(object))
            return convert(object);
        else if (object instanceof Collection) {
            Collection<Object> col = new LinkedList<Object>();
            for (Object o : (Collection<?>) object) {
                col.add(decodeFromMap(o));
            }
            return col;
        } else if (object instanceof Map<?, ?>) {
            @SuppressWarnings("unchecked")
            Map<String, Object> map = (Map<String, Object>) object;
            Object className = map.get(CLASS_KEY);
            if (className == null) { // This is a real attribute map
                Map<String, Object> decoded = new HashMap<>();
                for (Map.Entry<String, Object> entries : map.entrySet()) {
                    decoded.put(entries.getKey(), decodeFromMap(entries.getValue()));
                }
                return decoded;
            } else if (object instanceof Date)
                return object;
            else {
                try {
                    Class<?> clazz = Class.forName((String) className);
                    Object result = null;
                    if (clazz.isEnum()) {
                        String enumVal = (String) map.get(VALUE_KEY);
                        for (Object e : clazz.getEnumConstants()) {
                            if (e.toString().equals(enumVal))
                                return e;
                        }
                        return null;
                    } else {
                        result = clazz.newInstance();
                        for (Method method : clazz.getMethods()) {
                            String name = method.getName();
                            Type[] types = method.getGenericParameterTypes();
                            // Calling setters
                            if (name.startsWith("set") && name.length() > 3 && types.length == 1) {
                                name = name.substring(3, 4).toLowerCase() + name.substring(4);
                                if (map.containsKey(name)) {
                                    method.invoke(result, decodeFromMap(map.get(name)));
                                }
                            }
                        }
                    }
                    return result;
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        } else if (object.getClass().isArray()) { // Should not be used with DiaSpec but we never now
            Object[] o = (Object[]) object;
            Object[] array = new Object[o.length];
            for (int i = 0; i < o.length; i++) {
                array[i] = decodeFromMap(o[i]);
            }
            return array;
        }
        return null;
    }

    /**
     * Decode a json object from a reader
     * 
     * @param reader
     *            The reader to read the object from
     * @return The decoded object
     * @throws IOException
     */
    public static Object decodeJSON(BufferedReader reader) throws IOException {
        try {
            StringBuffer readed = new StringBuffer();
            if (reader != null) {
                char[] charBuffer = new char[128];
                int bytesRead = -1;
                while ((bytesRead = reader.read(charBuffer)) > 0) {
                    readed.append(charBuffer, 0, bytesRead);
                }
            } else {
                readed.append("");
            }
            return decodeJSON(readed.toString());
        } finally {
            if (reader != null)
                reader.close();
        }
    }
}
