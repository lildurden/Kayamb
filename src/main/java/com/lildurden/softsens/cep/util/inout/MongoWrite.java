package com.lildurden.softsens.cep.util.inout;

import com.lildurden.softsens.cep.subscriber.manager.Rule;
import com.lildurden.softsens.cep.event.SEvent;
import com.lildurden.softsens.cep.util.json.JSONUtils;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.FindIterable;
import com.mongodb.MongoException;

import org.bson.Document;
import static com.mongodb.client.model.Filters.eq;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Iterator;
import java.util.Set;
import java.util.HashMap;

public final class MongoWrite{
    private static final MongoWrite inst= new MongoWrite();
    private static String host="alerting_mongoasdas";
    private static int port=27017;
    private static String dbname_rule="rules";
    private static String dbname_streams="Streams4";
    private static final String fileName = "/opt/Sati/db.json";

    private static void initdbParam(){
	List<String> file_=new ArrayList<String>();
	file_=Files.getInstance().read(fileName);
	String file="";
	if(file_!=null){
	    for (int i=0;i<file_.size();++i){
		file+=file_.get(i);
	    }
	    Object jsonData = new HashMap<Object, Object>();
	    jsonData=JSONUtils.decodeJSON(file);
	    if(!((HashMap<?, ?>)jsonData).isEmpty()){
		HashMap<String, ?> fileJson =(HashMap<String, ?>)jsonData;
		host=fileJson.get("mongo_host").toString();
		port=Integer.parseInt(fileJson.get("mongo_port").toString());
		dbname_streams=fileJson.get("streams_collection").toString();
		dbname_rule=fileJson.get("rules_collection").toString();
	    }
	}
    }
    
    private MongoWrite(){
	super();	
    }
    
    public static MongoWrite getInstance() {
	initdbParam();
        return inst;
    }

    public static ArrayList<Rule> readRules(){
	ArrayList<Rule> rules=new ArrayList<Rule>();
	try{
	    MongoClient mongoClient = new MongoClient(host,port);
	    MongoDatabase database = mongoClient.getDatabase(dbname_rule);
	    MongoCollection<Document> collection =
		database.getCollection("global");
	    MongoCursor<Document> cursor=collection.find().iterator();

	    try {
		int i=0;
		while (cursor.hasNext()) {
		    //System.out.println(i++);
		    Document doc=cursor.next();
		    ArrayList<String> expr=new ArrayList<String>();
		    expr=doc.get("expressions",
				 ArrayList.class);
		    Rule rule=new Rule(doc.getString("name"),
				       expr);
		    rules.add(rule);		
		}
	    } finally {
		cursor.close();
	    }
	    mongoClient.close();
	}catch(MongoException e) {
	    System.out.println(e.getMessage());
	    e.printStackTrace();
	    return null;
	}
        return rules;
    }
    
    public static int deleteRule(String ruleName){
	try{
	    MongoClient mongoClient = new MongoClient(host,port);
	    MongoDatabase database = mongoClient.getDatabase(dbname_rule);
	    MongoCollection<Document> collection =
		database.getCollection("global");
	    collection.deleteOne(eq("name", ruleName));
	    mongoClient.close();
	}catch(MongoException e) {
	    System.out.println(e.getMessage());
	    e.printStackTrace();
	    return -1;
	}
	return 0;
    }
    public static int writeRule(Rule rule){
	try{
	    MongoClient mongoClient = new MongoClient(host,port);
	    MongoDatabase database = mongoClient.getDatabase(dbname_rule);
	    MongoCollection<Document> collection =
		database.getCollection("global");
	    ArrayList<String> expression=new ArrayList<String>();
	    expression=rule.getExpression();

	    Document doc=new Document("name",rule.getName())
		.append("expressions",expression);
	    collection.deleteOne(eq("name", rule.getName()));
	    collection.insertOne(doc);
	    mongoClient.close();
	    //System.out.println(doc.toJson());
	}catch(MongoException e) {
	    System.out.println(e.getMessage());
	    e.printStackTrace();
	    return -1;
	}
	return 0;
    }
    public static int writeRuleResults(Map<String,Object> eventMap,
				       String title){
	//System.out.println(title);
	try{
	    Set keys = eventMap.keySet();
	    Iterator it = keys.iterator();
	    String user="";
	    long timestamp_tmp=0;
	    ArrayList event_list = new ArrayList();
	    while(it.hasNext()){
		Object key= it.next();
		if(eventMap.get(key)!=null){
		    //System.out.println(eventMap.get(key).getClass().getName());
		    Document role = new Document("location",((SEvent)eventMap.get(key)).getRole().getLocation())
			.append("kind",((SEvent)eventMap.get(key)).getRole().getType());
		    Document _event = new Document("role",role)
			.append("status",((SEvent)eventMap.get(key)).getStatus_())
			.append("timestamp",((SEvent)eventMap.get(key)).getTimestamp())
			.append("user",((SEvent)eventMap.get(key)).getUser());
		
		    Document whole_event = new Document(((SEvent)eventMap.get(key)).getMyKind(),_event);
		
		    user=((SEvent)eventMap.get(key)).getUser();
		    timestamp_tmp=((SEvent)eventMap.get(key)).getTimestamp();
		    event_list.add(whole_event);
		
		}
	    }
	
	    MongoClient mongoClient = new MongoClient(host,port);
	    MongoDatabase database = mongoClient.getDatabase(dbname_streams);
	    MongoCollection<Document> collection = database.getCollection("_"+user);
	    Document doc = new Document("rule", title)
		//realtime deployement
		.append("timestamp", System.currentTimeMillis())
		//test deployement
		//.append("timestamp", timestamp_tmp)
		.append("events", event_list);
	
	    //System.out.println(doc.toJson());
	    collection.insertOne(doc);
	    mongoClient.close();
	}catch(MongoException e) {
	    System.out.println(e.getMessage());
	    e.printStackTrace();
	    return -1;
	}
	return 0;
    }

}
