package com.lildurden.softsens.cep.util.jni;
import java.io.Serializable;

public class JNIParser implements Serializable{
    private static final long serialVersionUID = 1L;
    private native Object[] set_log(long object,String user_,String date_,String type_,String location_ ,String status_);
    private native long init_parser();    
    // private native Object[] get_last(long object,String user_,String type_, String location_,long timestamp_);
    private long librole;
    
    public JNIParser(){
	NativeLoader nl=new NativeLoader();
	nl.loadLibrary("Djembe");
	librole=init_parser();
    }
    public Object[] set_log(String user_, String date_, String type_, String location_,String status_){
	return set_log(librole,user_,date_,type_,location_,status_);
    }
    
    // public Object[] get_last_stream(String user_, String type_, String location_,long timestamp_){
    // 	return get_last(librole,user_,type_,location_,timestamp_);
    // }
}
