package com.lildurden.softsens.cep.util;

import com.lildurden.softsens.cep.event.SEvent;
import com.lildurden.softsens.cep.event.StreamEvent;
import com.lildurden.softsens.cep.event.Role;
import java.util.ArrayList;
import java.util.ListIterator;
import java.util.Calendar; 
import java.util.Map;
import java.util.HashMap;

public class DoCalendar{
    
    private Map<String,Map<String,Boolean> > calendars;
    //private ArrayList<SEvent> arevents=new ArrayList<SEvent>();
    public DoCalendar(){
	calendars=new HashMap<String,Map<String,Boolean> >();
    }
    private long doDaily(long timestamp){
	Calendar c = Calendar.getInstance();
	c.setTimeInMillis(timestamp);
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return (timestamp-c.getTimeInMillis());
    }
    private void addCal(String calName,
			boolean begin,
			long timestamp,
			String user,ArrayList<SEvent> arevents){
	if(calendars.get(user)==null){
	    Map<String,Boolean> cal=new HashMap<String,Boolean>();
	    cal.put(calName,begin);
	    calendars.put(user,cal);
	    arevents.add(new StreamEvent("Calendar",calName,
					 begin?"begin":"end",
					 timestamp,user));
	}else{
	    if(calendars.get(user).get(calName)==null){
		calendars.get(user).put(calName,begin);
	        arevents.add(new StreamEvent("Calendar",calName,
					     begin?"begin":"end",
					     timestamp,user));
	    }else{
		if(calendars.get(user).get(calName)!=begin){
		    calendars.get(user).put(calName,begin);
		    arevents.add(new StreamEvent("Calendar",calName,
						 begin?"begin":"end",
						 timestamp,user));
		}
	    }    					      		 
	}
    }
    private void doCal(long begin,
		       long end,
		       long daily,
		       long timestamp,
		       String calName,
		       SEvent se,ArrayList<SEvent> arevents){
	if(begin<end){
	    if(daily>=begin&&daily<=end)
		addCal(calName,true,timestamp,se.getUser(),arevents);
	    if(daily<begin||daily>end)
		addCal(calName,false,timestamp,se.getUser(),arevents);
	}else{
	    if(daily>=begin||daily<=end)
		addCal(calName,true,timestamp,se.getUser(),arevents);
	    if(daily<begin&&daily>end)
		addCal(calName,false,timestamp,se.getUser(),arevents);
	}
    }
    public synchronized ArrayList<SEvent> doIt(SEvent se){
	ArrayList<SEvent> arevents=new ArrayList<SEvent>();
	//arevents.clear();
	//System.out.println(se);
	long timestamp=se.getTimestamp();
        long daily = doDaily(timestamp);
	//	System.out.println(daily);
	String user=se.getUser();
	switch(user){
	case "Domassist5006":
	    //wakeup
	    doCal(27000000,30600000,daily,timestamp,"WakeUp",se,arevents);
	    //breakfast
	    doCal(27000000,32400000,daily,timestamp,"Breakfast",se,arevents);
	    //lunch
	    doCal(41400000,48600000,daily,timestamp,"Lunch",se,arevents);
	    //dinner
	    doCal(68400000,72000000,daily,timestamp,"Dinner",se,arevents);
	    //gobed
	    doCal(79200000,1800000,daily,timestamp,"Bed",se,arevents);
	    //night
	    doCal(1800000,27000000,daily,timestamp,"Night",se,arevents);
	    //dressing
	    doCal(27000000,41400000,daily,timestamp,"Dressing",se,arevents);
	    break;
	case "DomassistBauzet":
	    doCal(21600000,28800000,daily,timestamp,"WakeUp",se,arevents);
	    doCal(27000000,34200000,daily,timestamp,"Breakfast",se,arevents);
	    doCal(39600000,48600000,daily,timestamp,"Lunch",se,arevents);
	    doCal(64800000,75600000,daily,timestamp,"Dinner",se,arevents);
	    doCal(64800000,79200000,daily,timestamp,"Bed",se,arevents);
	    doCal(79200000,21600000,daily,timestamp,"Night",se,arevents);
	    doCal(21600000,39600000,daily,timestamp,"Dressing",se,arevents);
	    break;
	case "Domassist13005":
	    doCal(21600000,34200000,daily,timestamp,"WakeUp",se,arevents);
	    doCal(21600000,34200000,daily,timestamp,"Breakfast",se,arevents);
	    doCal(39600000,48600000,daily,timestamp,"Lunch",se,arevents);
	    doCal(64800000,77400000,daily,timestamp,"Dinner",se,arevents);
	    doCal(72000000,0,daily,timestamp,"Bed",se,arevents);
	    doCal(0,21600000,daily,timestamp,"Night",se,arevents);
	    doCal(21600000,39600000,daily,timestamp,"Dressing",se,arevents);
	    break;
	case "Domassist116":
	    doCal(21600000,27000000,daily,timestamp,"WakeUp",se,arevents);
	    doCal(27000000,34200000,daily,timestamp,"Breakfast",se,arevents);
	    doCal(39600000,48600000,daily,timestamp,"Lunch",se,arevents);
	    doCal(68400000,73800000,daily,timestamp,"Dinner",se,arevents);
	    doCal(79200000,82800000,daily,timestamp,"Bed",se,arevents);
	    doCal(82800000,21600000,daily,timestamp,"Night",se,arevents);
	    doCal(21600000,39600000,daily,timestamp,"Dressing",se,arevents);
	    break;
	case "Domassist117":
	    doCal(21600000,32400000,daily,timestamp,"WakeUp",se,arevents);
	    doCal(21600000,36000000,daily,timestamp,"Breakfast",se,arevents);
	    doCal(39600000,48600000,daily,timestamp,"Lunch",se,arevents);
	    doCal(63000000,79200000,daily,timestamp,"Dinner",se,arevents);
	    doCal(79200000,5400000,daily,timestamp,"Bed",se,arevents);
	    doCal(5400000,21600000,daily,timestamp,"Night",se,arevents);
	    doCal(21600000,39600000,daily,timestamp,"Dressing",se,arevents);
	    break;
	case "Domassist4022":
	    doCal(23400000,28800000,daily,timestamp,"WakeUp",se,arevents);
	    doCal(25200000,30600000,daily,timestamp,"Breakfast",se,arevents);
	    doCal(43200000,46800000,daily,timestamp,"Lunch",se,arevents);
	    doCal(64800000,72000000,daily,timestamp,"Dinner",se,arevents);
	    doCal(78300000,82800000,daily,timestamp,"Bed",se,arevents);
	    doCal(82800000,23400000,daily,timestamp,"Night",se,arevents);
	    doCal(23400000,43200000,daily,timestamp,"Dressing",se,arevents);
	    break;
	case "Domassist4010":
	    doCal(21600000,30600000,daily,timestamp,"WakeUp",se,arevents);
	    doCal(25200000,34200000,daily,timestamp,"Breakfast",se,arevents);
	    doCal(39600000,46800000,daily,timestamp,"Lunch",se,arevents);
	    doCal(66600000,75600000,daily,timestamp,"Dinner",se,arevents);
	    doCal(79200000,84600000,daily,timestamp,"Bed",se,arevents);
	    doCal(84600000,21600000,daily,timestamp,"Night",se,arevents);
	    doCal(21600000,39600000,daily,timestamp,"Dressing",se,arevents);
	    break;
	case "Domassist4020":
	    doCal(30600000,39600000,daily,timestamp,"WakeUp",se,arevents);
	    doCal(32400000,39600000,daily,timestamp,"Breakfast",se,arevents);
	    doCal(43200000,50400000,daily,timestamp,"Lunch",se,arevents);
	    doCal(72000000,81000000,daily,timestamp,"Dinner",se,arevents);
	    doCal(79200000,5400000,daily,timestamp,"Bed",se,arevents);
	    doCal(5400000,30600000,daily,timestamp,"Night",se,arevents);
	    doCal(30600000,43200000,daily,timestamp,"Dressing",se,arevents);
	    break;
	default:
	    doCal(25200000,32400000,daily,timestamp,"WakeUp",se,arevents);
	    doCal(25200000,32400000,daily,timestamp,"Breakfast",se,arevents);
	    doCal(28800000,46800000,daily,timestamp,"Lunch",se,arevents);
	    doCal(28800000,46800000,daily,timestamp,"Dinner",se,arevents);
	    doCal(75600000,0,daily,timestamp,"Bed",se,arevents);
	    doCal(0,75600000,daily,timestamp,"Night",se,arevents);
	    doCal(21600000,39600000,daily,timestamp,"Dressing",se,arevents);
	    break;
	};
	

	return arevents;

    }
}
