package com.lildurden.softsens.cep.util.inout;

import java.util.Map;
import java.util.Iterator;
import java.util.Set;
import java.util.ArrayList;

import com.lildurden.softsens.cep.event.SEvent;
import com.lildurden.softsens.cep.http.SendHttpRequest;

public class Output{
    public static void write(String filepath, String message){
	Files.getInstance().writeNext(filepath,message);
    }
    
    public static void writeFile(Map<String,Object> eventMap,
				 String title){
	Set keys = eventMap.keySet();
	Iterator it = keys.iterator();
	String user="";
	String events="\"events\":[";
	String res="{\"rule\":\""+title+"\",\"timestamp\":"; //+event.getTimestamp()+",";
	long timestamp=0;
	//res+= "\"events\":["+event.toString()+"]}";
	//String user=event.getUser();	
	//res+="\n";
	while(it.hasNext()){
	    Object key= it.next();
	    if(eventMap.get(key)!=null){
		events+=eventMap.get(key).toString()+",";
		user=((SEvent)eventMap.get(key)).getUser();
		long timestamp_tmp=((SEvent)eventMap.get(key)).getTimestamp();
		if(timestamp_tmp>timestamp)
		    timestamp=timestamp_tmp;	
		
	    }
	}
	events=events.substring(0,events.lastIndexOf(","));
	events+="]";
	
	//res+="\n";
	
	//set current timestamp!
	//res+=timestamp;//
	res+=System.currentTimeMillis();
	res+=",";
	res+=events;
	res+="}";
	//System.out.println(res);
	//res=eventMap.toString()+"\n";
	//System.out.println(user+"\n\n");
	//Output.write("/opt/webapp/" + user+"/" + title,res);
	
	// SendHttpRequest sHttp=new SendHttpRequest("http://localhost",
	// 					  "9089",
	// 					  "Events/"+title);
	// sHttp.sendPost(res);
	
    }
    
    public static void BsonRuleSaveResults(Map<String,Object> eventMap,
					   String title){
	//System.output.println(title);
	MongoWrite.getInstance().writeRuleResults(eventMap,title);
    }
    public static void writeFile(SEvent event){
	String res="{\"rule\":\"raw\",\"timestamp\":"+event.getTimestamp()+",";
	res+= "\"events\":["+event.toString()+"]}";
	String user=event.getUser();	
	res+="\n";
	//Output.write("/opt/webapp/" + user+"/" + "raw",res);
	// SendHttpRequest sHttp=new SendHttpRequest("http://localhost",
	// 					  "8989",
	// 					  "Events/raw");
	// sHttp.sendPost(res);
    }
    public static void sendEvent(SEvent event){
	SendHttpRequest lol=new SendHttpRequest();
	String res=event.toString();
	String user=event.getUser();
	res+="\n";
	String message="{user:\""+user+"\""+res+"}";
	lol.sendPut(message);
    }
    public static void sendEvent(Map<String,SEvent> eventMap,String date,String ruleName){	
	SendHttpRequest httpReq=new SendHttpRequest();	
	Set keys = eventMap.keySet();
	Iterator it = keys.iterator();
	String res="";
	String user="";	
	while(it.hasNext()){
	    Object key= it.next();
	    res+=eventMap.get(key);//+"\n";
	    user=eventMap.get(key).getUser();
	}
	res+="\n";
	String message="{\"_source\":{\"@timestamp\":\""+date+"\",\"source\":\"rule\",\"name\":\""+ruleName+"\",\"user\":\""+user+"\",\"role\":"+res+"}}";
	httpReq.sendPut(message);
    }
}
