package com.lildurden.softsens.cep.event.manager;

import com.lildurden.softsens.cep.util.json.JSONUtils;
//import com.lildurden.softsens.cep.util.jni.JNIParser;
import com.lildurden.softsens.cep.handler.RoleEventHandler;
import com.lildurden.softsens.cep.event.manager.Parser;
import com.lildurden.softsens.cep.event.SEvent;
import com.lildurden.softsens.cep.event.StreamEvent;
import com.lildurden.softsens.cep.event.CommFailure;
import com.lildurden.softsens.cep.event.BatteryLevel;
import com.lildurden.softsens.cep.http.Decode;
import com.lildurden.softsens.cep.subscriber.manager.RuleManager;
import com.lildurden.softsens.cep.subscriber.manager.Rule;
import com.lildurden.softsens.cep.util.json.JSONUtils;
import com.lildurden.softsens.cep.util.DoCalendar;

import java.util.Map;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.commons.lang3.StringUtils ;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DispatchRole{
    private static Logger LOG = LoggerFactory.getLogger(DispatchRole.class);
    
    //private JNIParser parse;
    private String id;
    private RoleEventHandler roleEventHandler;

    private DoCalendar doCal;
    
    public DispatchRole(RoleEventHandler roleEventHandler){
	this.roleEventHandler=roleEventHandler;
    }

    public DispatchRole(RoleEventHandler roleEventHandler,
			String id,
			DoCalendar doCal){
	this.roleEventHandler=roleEventHandler;
	this.id=id;
	this.doCal=doCal;
    }
    
    public DispatchRole(RoleEventHandler roleEventHandler,String id){
	this.roleEventHandler=roleEventHandler;
    	this.id=id;
    }
    public DispatchRole(String id){
	this.roleEventHandler=new RoleEventHandler(id);
	//this.parse=new JNIParser();	
    	this.id=id;
    }

    private String setTime(HashMap<?, ?> sourceMap){
	String time= sourceMap.get("@timestamp").toString();//.replace('T', ' ');
	//time=time.substring(0,time.length()-1);
	return time;
    }
    
    private void handleEvent(SEvent event){
	roleEventHandler.handle(event);
    }

    private void distribution(String kindStream,
			      String locationStream,
			      String statusStream,
			      long timestampStream){
	switch(kindStream){
	case "BatteryLevel":
	    roleEventHandler.handle(new BatteryLevel(kindStream,
						    locationStream,
						    //+subloc,
						    statusStream,
						    timestampStream,
						    id
						    )
				    );
	    break;
	case "CommFailure":
	    roleEventHandler.handle(new CommFailure(kindStream,
						    locationStream,
						    //+subloc,
						    statusStream,
						    timestampStream,
						    id
						    )
				    );
	    break;
	default:
	    roleEventHandler.handle(new StreamEvent(kindStream,
						    locationStream,
						    //+subloc,
						    statusStream,
						    timestampStream,
						    id
						    )
				    );
	    break;
	}
    }
    
    private synchronized void parseData(String time,
			   String kind,
			   String location,
			   String status){

	
	// // String kindStream="kind";//roleEvent[0].toString();
	// // String locationStream="location";//roleEvent[1].toString();
	// // String statusStream="status";//roleEvent[2].toString();
	// // long timestampStream=0;


	
	
	Parser parse=new Parser();
	SEvent sevent=parse.parse(time,kind,location,status,id);
	ArrayList<SEvent> arevents=doCal.doIt(sevent);
	Iterator<SEvent> itr = arevents.iterator();
	while (itr.hasNext()) {
	    SEvent lol=itr.next();
	    //System.out.println(lol);
	    roleEventHandler.handle(lol);
	}
	Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        //System.out.println( sdf.format(cal.getTime()) );
	SEvent sevent2=parse.parse(sdf.format(cal.getTime()),kind,location,status,id);
	roleEventHandler.handle(sevent2);
	
	// Object[] roleEvent=parse.set_log(id,time,kind,location,status);
	// String kindStream=roleEvent[0].toString();
	// String locationStream=roleEvent[1].toString();
	// String statusStream=roleEvent[2].toString();
	// long timestampStream=Long.parseLong(roleEvent[3].toString());
	
	// // System.out.println("RoleEvent "+kindStream+" "
	// // 		   + locationStream+" "
	// // 		   + statusStream+" "
	// // 		   +timestampStream+" "+id);
	
	// if(!kindStream.equals("")&&
	//    !locationStream.toString().equals("")&&
	//    !statusStream.toString().equals("")
	//    ){
	//     distribution(kindStream,
	// 		 locationStream,
	// 		 //+subloc,
	// 		 statusStream,
	// 		 timestampStream);
	//     // roleEventHandler.handle(new StreamEvent(kindStream,
	//     // 					    locationStream,
	//     // 					    //+subloc,
	//     // 					    statusStream,
	//     // 					    timestampStream,
	//     // 					    id
	//     // 					    )
	//     // 			    );
	// }
    }
    
    public synchronized String dispatchPut(String text){
	String returnString="invalid request";
	returnString = setLog(text);
	return returnString;
    }
    /**
     * Set body from setlog request
     */
    private String setLog(String text){
	HashMap<?, ?> bodyJson_ = Decode.getJsonRequest(text);
	HashMap<?, ?> bodyJson =(bodyJson_.get("_source")!=null?(HashMap<?, ?>)bodyJson_.get("_source"):(HashMap<?, ?>)bodyJson_);
	///LOG.debug(bodyJson.toString());
	if(bodyJson!=null
	   //&&(HashMap<?, ?>)bodyJson.get("_source")!=null
	   ){
	    if(bodyJson.get("type")!=null){
		return dispatchByType((HashMap<?, ?>)bodyJson);
	    }
	    return dispatchOthers((HashMap<?, ?>)bodyJson);
	}else
	    return "<fail:convertjson>";
    }
    private String dispatchByType(HashMap<?, ?> sourceMap){
	String type=sourceMap.get("type").toString().toLowerCase();
	String result="great";
	switch(type){
	case "event":
	    setEvents(sourceMap);
	    break;
	case "healthcheck":
	    setHealthcheck(sourceMap);
	    break;
	case "heartbeat":
	    setHeartbeat(sourceMap);
	    break;
	// case "rule":
	//     result=setRule(RuleManager.setRule(sourceMap));
	//     break;
	case "devices":
	    result=setDevice(sourceMap);
	    break;
	default:
	    break;
	}
	return result;
    }
    private String dispatchOthers(HashMap<?, ?> sourceMap){
	String source=sourceMap.get("source").toString().toLowerCase();
	String location="";
	String status="";
	String time=setTime(sourceMap);
	//     sourceMap.get("@timestamp").toString().replace('T', ' ');
	// time=time.substring(0,time.length()-1);	
	
	switch(source){
	case "userbox":
	    if(sourceMap.get("tablet_uuid")!=null){
		location=sourceMap.get("tablet_uuid").toString();
		status=sourceMap.get("logger_name").toString()+"|"+sourceMap.get("action").toString();
		//System.out.println(status+" "+location);
	    }
	    break;
	case "tablet":
	    location=sourceMap.get("apk").toString();
	    if(sourceMap.get("content").toString().equals("notification")){
		HashMap<?, ?> notification = (HashMap<?, ?>)sourceMap.get("notification");
		if(notification.get("notification")!=null)
		    status=notification.get("notification").toString();
		else{
		    if(notification.get("operation")!=null){
			status=notification.get("operation").toString();
		    }else{
			status=sourceMap.get("content").toString();
		    }
		}
	    }else{
		if(sourceMap.get("content").toString().equals("global")){
		    HashMap<?, ?> notification = (HashMap<?, ?>)sourceMap.get("global");
		    status=notification.get("action").toString();
		}else{
		    status=sourceMap.get("content").toString();
		}
	    }
	    break;
	case "rule":
	    location=sourceMap.get("name").toString();
	    status="rule";
	default:
	    break;
	}
	if(!location.equals("")&&!status.equals("")){
	    //System.out.println(status+" "+location);
	    parseData(time, source, location, status);
	}
	return "great";
    }

    private String checkRoom(HashMap<?, ?> sourceMap){
	return (((HashMap<?, ?>)sourceMap.get("device")).get("room")!=null ? ((HashMap<?, ?>)sourceMap.get("device")).get("room").toString():"unknown");
    }
    private boolean checkOldValue(HashMap<?, ?> sourceMap){
	if(((HashMap<?, ?>)sourceMap.get("event")).get("value")!=null&&
	   ((HashMap<?, ?>)sourceMap.get("event")).get("old_value")!=null)
	    return !((HashMap<?, ?>)sourceMap.get("event")).get("value").toString().equals(((HashMap<?, ?>)sourceMap.get("event")).get("old_value").toString());
	
	return true;
    }
    private String setDevice(HashMap<?, ?>  sourceMap){
	String time=setTime(sourceMap);
	    //  sourceMap.get("@timestamp").toString().replace('T', ' ');
	// //This is dirty
	// time=time.replace('Z', ' ');
	if(((HashMap<String, ?>)sourceMap.get("device")).containsKey("batterylevel")){
	    parseData(time,
		      "BatteryLevel",
		      checkRoom(sourceMap)+"|"+((HashMap<?, ?>)sourceMap.get("device")).get("name").toString(),
		      ((HashMap<?, ?>)sourceMap.get("device")).get("batterylevel").toString());
	}
	
	if(((HashMap<String, ?>)sourceMap.get("device")).containsKey("commfailure")){
	    parseData(time,
		      "CommFailure",
		      checkRoom(sourceMap)+"|"+((HashMap<?, ?>)sourceMap.get("device")).get("name").toString(),
		      ((HashMap<?, ?>)sourceMap.get("device")).get("commfailure").toString());
	}
	return "great";
    }
    
    private String setEvents(HashMap<?, ?> sourceMap){
	String time=setTime(sourceMap);
	    // sourceMap.get("@timestamp").toString().replace('T', ' ');
	// //This is dirty
	// time=time.replace('Z', ' ');
	switch(((HashMap<?, ?>)sourceMap.get("event")).get("variable").toString().toLowerCase()){
	case "commfailure":
	    if(checkOldValue(sourceMap))
		parseData(time,
			  ((HashMap<?, ?>)sourceMap.get("event")).get("variable").toString(),
			  checkRoom(sourceMap)+"|"+((HashMap<?, ?>)sourceMap.get("device")).get("name").toString(),
			  ((HashMap<?, ?>)sourceMap.get("event")).get("value").toString());
		// parseData(time,
		// 	  ((HashMap<?, ?>)sourceMap.get("event")).get("variable").toString(),
		// 	  checkRoom(sourceMap)+"|"+((HashMap<?, ?>)sourceMap.get("device")).get("name").toString(),
		// 	  ((HashMap<?, ?>)sourceMap.get("event")).get("value").toString());
	    break;
	case "batterylevel":
	    if(checkOldValue(sourceMap))
		parseData(time,
			  ((HashMap<?, ?>)sourceMap.get("event")).get("variable").toString(),
			  checkRoom(sourceMap)+"|"+((HashMap<?, ?>)sourceMap.get("device")).get("name").toString(),
			  ((HashMap<?, ?>)sourceMap.get("event")).get("value").toString());
	    break;
	case "watts":
	case "tripped":
	case "other":
	    if(checkOldValue(sourceMap)){
		parseData(time,
			  ((HashMap<?, ?>)sourceMap.get("device")).get("name").toString(),
			  checkRoom(sourceMap),
			  ((HashMap<?, ?>)sourceMap.get("event")).get("value").toString());
	    }
	break;
	default:
	    break;
	}
	return "great";
    }
    /**
     * Set body from healthcheck request
     */
    private String setHealthcheck(HashMap<?, ?> sourceMap){
	String user=sourceMap.get("user")!=null?sourceMap.get("user").toString():"";
	String time=setTime(sourceMap);
	    // sourceMap.get("@timestamp").toString().replace('T', ' ');
	// time=time.substring(0,time.length()-1);
	parseData(time,
		  "memory",
		  sourceMap.get("source").toString(),
		  sourceMap.get("used_memory_percent").toString());
	parseData(time,
		  "cpu",
		  sourceMap.get("source").toString(),
		  sourceMap.get("cpu").toString());
	if(sourceMap.get("source").toString().equals("userbox")){
	    parseData(time,
		      "logged_error",
		      sourceMap.get("source").toString(),
		      sourceMap.get("logged_error").toString());
	}
	return "great";		       
    }
    /**
     * Set body from heartbeat request
     */
    private String setHeartbeat(HashMap<?, ?> sourceMap){
	String user=sourceMap.get("user")!=null?sourceMap.get("user").toString():"";
	String time=setTime(sourceMap);
	    // sourceMap.get("@timestamp").toString().replace('T', ' ');
	// time=time.substring(0,time.length()-1);	
	parseData(time,
		  sourceMap.get("type").toString(),
		  sourceMap.get("vera_serial").toString(),
		  sourceMap.get("type").toString());
	return "great";		       
    }
    // public void addRule(Rule rule){
    //  	roleEventHandler.addRule(rule);
    // }
    // public String setRule(Rule rule){
    // 	if(rule.getForceWrite()){
    // 	    if(roleEventHandler.addRuleForced(rule)==0){
    // 	    	RuleManager.write_forced(rule.getName(),
    // 	    				 rule.getExpression(),
    // 	    				 rule.getId());
    // 	    }else{
    // 	    	return "error "+rule.getName()+": "+rule.getExpression();
    // 	    }
    // 	}else{
    // 	    addRule(rule);
    // 	    RuleManager.write(rule.getName(),
    // 			      rule.getExpression(),
    // 			      rule.getId());
    // 	}
    // 	return "great";
    // }
}

 
