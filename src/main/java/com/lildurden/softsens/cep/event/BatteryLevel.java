package com.lildurden.softsens.cep.event;
import com.lildurden.softsens.cep.util.json.JSONUtils;
import java.util.Map;
import java.util.HashMap;

public class BatteryLevel extends SEvent{

    public BatteryLevel(BatteryLevel tocpy){
	super(tocpy.role.getType(),
	      tocpy.role.getLocation(),
	      tocpy.status,
	      tocpy.getTimestamp(),
	      tocpy.getUser());
    }
    public BatteryLevel(String type,String location, String status, long timestamp,String user) {
	super(type,location,status,timestamp,user);
	//	System.out.println(location);
	HashMap<?, ?> bodyJson = getJsonRequest("{"+location+"}");
	HashMap<String, ?> bodyMap=(HashMap<String, ?>)bodyJson.get("Role");
	String kind_=bodyMap.get("kind").toString();
	String location_=bodyMap.get("location").toString();
	role=new Role(kind_,location_);
    }
    
    public BatteryLevel(String type,Role role_, String status, long timestamp,String user) {
	super(type,role_.toString(),status,timestamp,user);
        role=new Role(role_);
    }
    public String getMyKind(){return "BatteryLevel";}
    public float getStatus(){return Float.parseFloat(status);}
    @Override
    public String toString() {
	return "{\"BatteryLevel\":{"+role+",\"status\":\""+status+"\",\"timestamp\":\""+timestamp+"\",\"user\":\""+user+"\"}}";
    }
    private static HashMap<?, ?> getJsonRequest(String text){
	Object jsonData = new HashMap<Object, Object>();
	jsonData=JSONUtils.decodeJSON(text);
	if(!((HashMap<?, ?>)jsonData).isEmpty()){
	    return (HashMap<?, ?>)jsonData;
	}
	return null;		
    }
}
