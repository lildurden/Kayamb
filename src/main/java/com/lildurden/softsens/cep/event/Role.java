package com.lildurden.softsens.cep.event;

public class Role {

    private String location;
    private String type;
    private String sublocation="";
    public Role(Role tocpy) {
	this.type = tocpy.type;
	this.location=tocpy.location;
    }
    public Role(String type,String location) {
	this.type = type;
	int index=location.lastIndexOf('-');	
	this.sublocation=(index==-1)?"":(location.substring(index+1));
	this.location=(index==-1)?location:(location.substring(0,index));
	//System.out.println("lolilol   "+this.location+" "+this.sublocation+" "+this.type);
    }
    public String getLocation() {
        return location;
    }
    public String getType() {
        return type;
    }
    public boolean equals(Role r1){
	return location.equals(r1.location) && type.equals(r1.type);
    }
    @Override
    public String toString() {
	return "\"Role\":{\"kind\":\""+type+"\",\"location\":\""+location+(sublocation.equals("")?"":" "+sublocation)+"\"}";
    }

}
