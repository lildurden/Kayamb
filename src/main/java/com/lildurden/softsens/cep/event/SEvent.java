package com.lildurden.softsens.cep.event;

public class SEvent extends RoleEvent{

    protected String status;
    protected long timestamp;
    protected SEvent lastSe;

    public SEvent(SEvent tocpy){
	super(tocpy.role.getType(),
	      tocpy.role.getLocation(),
	      tocpy.getUser());
	this.status=tocpy.status;
	this.timestamp=tocpy.timestamp;
    }
    public SEvent(String type,String location, String status, long timestamp,String user) {
	super(type,location,user);
	this.status=status.replace("\n", "").replace("\r", "");
	this.status=this.status.replace("\"", "");
	this.status=this.status.replace("\t", "");
	//this.status=this.status.replace(" ", "_");
	this.timestamp=timestamp;
    }
    public long getTimestamp(){return timestamp;}
    
    public String getStatus_(){return status;}
    public boolean equals(StreamEvent se){
	if(lastSe!=null)
	    return this.status==se.status &&
		this.timestamp==se.timestamp&&
		this.role.equals( se.role);
	return false;
    }
    public String getMyKind(){return "SEvent";}
    @Override
    public String toString() {
	return "{\"SEvent\":{"+role+",\"status\":\""+status+"\",\"timestamp\":\""+timestamp+"\",\"user\":\""+user+"\"}}";
    }

}
