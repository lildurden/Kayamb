package com.lildurden.softsens.cep.event.manager;

import com.lildurden.softsens.cep.event.SEvent;
import com.lildurden.softsens.cep.event.StreamEvent;
import com.lildurden.softsens.cep.event.CommFailure;
import com.lildurden.softsens.cep.event.BatteryLevel;
import com.lildurden.softsens.cep.event.Role;
import java.text.SimpleDateFormat;
import java.text.DateFormat; 
import java.util.Date;
import java.sql.Timestamp; 
import java.lang.NumberFormatException ;
// import java.util.Calendar;
// import java.util.GregorianCalendar;
import java.util.TimeZone;

public class Parser{
    private String kind="";
    private String location="";
    private String status="";

    private long stringToTimestamp(String date){
	try{
	    //System.out.println(date);
	    SimpleDateFormat formatter =
		new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
	    //formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
	    Date fromGmt=formatter.parse(date);
	    //System.out.println(formatter.format(fromGmt));

	    // SimpleDateFormat formatter2 =
	    // 	new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
	    // //Date timezone = formatter.parse("2012-04-14 14:23:34");
	    // formatter2.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
	    // //System.out.println(formatter2.format(fromGmt));

	    
	    // Date lFromDate1 = formatter2.parse(formatter2.format(fromGmt));
	    //Timestamp fromTS1 = new Timestamp(lFromDate1.getTime());
	    Timestamp fromTS1 = new Timestamp(fromGmt.getTime());  

	    return fromTS1.getTime();
	}catch (Exception ex){
	    System.out.println("Error reading text message:" + ex);
	    return 0;
	}
    }
    
    private void unsplit_kind_location(){
	int find_split=location.indexOf("|"); 
	String tmp_location=location.substring(0,find_split);
	String tmp_kind=location.substring(find_split+1,location.length());
	Role role=new Role(up_device_kind(tmp_kind),tmp_location);
	location=role.toString();
	//device.kind=tmp_kind;
    }
    
    private void up_device_status(){
	if(kind.compareTo("Presence")==0){
	    if(status.compareTo("0")==0){
		status="false";
	    }else{
		if(status.compareTo("1")==0)
		    status="true";
	    }
	}else{
	    if(kind.compareTo("Door")==0||
	       kind.compareTo("Cupboard")==0||
	       kind.compareTo("Fridge")==0){
		if(status.compareTo("0")==0)
		    status="close";
		else
		    status="open";
	    }else{
		if(kind.compareTo("Use")==0||
		   kind.compareTo("Coffeemaker")==0||
		   kind.compareTo("Oven")==0||
		   kind.compareTo("Kettle")==0||
		   kind.compareTo("Juicer")==0||
		   kind.compareTo("Meatcleaver")==0||
		   kind.compareTo("Toaster")==0){
		    if(status.equals(""))
			status="off";
		    else{
			try{
			if(Float.parseFloat(status)>1.0)
			    status="on";
			else
			    status="off";
			}catch(NumberFormatException e){
			    status="";
			}
		    }
		}else{   
		    if(kind.compareTo("Microwave")==0){
			try{

			if(Float.parseFloat(status)>10.0)
			    status="on";
			else
			    status="off";
			}catch(NumberFormatException e){
			    status="";
			    System.out.println(""+e);
			}
		    }else{
			if(kind.compareTo("CommFailure")==0){
			    unsplit_kind_location();
			    if(status.compareTo("1")==0||
			       status.compareTo("true")==0){
				status="true";
			    }else{
				if(status.compareTo("0")==0||
				   status.compareTo("false")==0){
				    status="false";
				}
			    }
			}else{
			    if(kind.compareTo("BatteryLevel")==0){
				unsplit_kind_location();
			    }else{
			    }
			}
		    }
		}
	    }
	}
    }
    
    private String up_device_kind(String kind){
	int find_composed_kind=kind.lastIndexOf("_");
        String composed_kind="";
	if(find_composed_kind!=-1){
	    String sub_kind=kind.substring(find_composed_kind+1,kind.length());
	    if(sub_kind.length()>3){
		composed_kind=sub_kind;
	    }
	}
	if(composed_kind.length()>1&&kind.compareTo("logged_error")!=0)
	    return composed_kind;
  
	if(kind.contains("Motion")){
	    return "Presence";
	}
	else{
	    if(kind.contains("Contact")){
		return "Door";
	    }
	    else{
		if(kind.contains("Electric")||
		   kind.contains("EMeter")){
		    return "Use";
		}else{
		    return kind;
		}
	    }
	}
    }

    
    public SEvent parse(String time,
			String kind,
			String location,
			String status,
			String id){
	this.location=location;
	this.status=status;
	this.kind=up_device_kind(kind);
	//System.out.println("kind: "+this.kind);
	up_device_status();
	// String time_tmp=time.replaceAll("T"," ");
	// time_tmp=time_tmp.replaceAll("Z","");
	long timestamp=stringToTimestamp(time/*_tmp*/);
	if(timestamp==0)
	    return null;
	
	SEvent sevent;

	switch(this.kind){
	case "BatteryLevel":
	    sevent=new BatteryLevel(this.kind,
				    this.location,
				    this.status,
				    timestamp,
				    id);
	    break;
	case "CommFailure":
	    sevent=new CommFailure(this.kind,
				   this.location,
				   this.status,
				   timestamp,
				   id);
	    break;
	default:
	    sevent=new StreamEvent(this.kind,
				   this.location,
				   this.status,
				   timestamp,
				   id);
	    break;
	}
	return sevent;
    }
}
