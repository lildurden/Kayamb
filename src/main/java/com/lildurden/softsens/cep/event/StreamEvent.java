package com.lildurden.softsens.cep.event;

public class StreamEvent extends SEvent{
    public StreamEvent(StreamEvent tocpy){
	super(tocpy);	
    }
    public StreamEvent(String type,String location, String status, long timestamp,String user) {
	super(type,location,status,timestamp,user);
    }
    public String getStatus(){return status;}
    public String getMyKind(){return "StreamEvent";}
    @Override
    public String toString() {
	return "{\"StreamEvent\":{"+role+",\"status\":\""+status+"\",\"timestamp\":\""+timestamp+"\",\"user\":\""+user+"\"}}";
    }

}
