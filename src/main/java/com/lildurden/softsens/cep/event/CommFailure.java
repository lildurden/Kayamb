package com.lildurden.softsens.cep.event;
import com.lildurden.softsens.cep.util.json.JSONUtils;
import java.util.Map;
import java.util.HashMap;

public class CommFailure extends SEvent{

    //private Role encaps_role;
    
    public CommFailure(CommFailure tocpy){
	super(tocpy.role.getType(),
	      tocpy.role.getLocation(),
	      tocpy.getStatus(),
	      tocpy.getTimestamp(),
	      tocpy.getUser());
    }
    public CommFailure(String type,String location, String status, long timestamp,String user) {
	super(type,location,status,timestamp,user);
	//System.out.println(location);
	HashMap<?, ?> bodyJson = getJsonRequest("{"+location+"}");
	HashMap<String, ?> bodyMap=(HashMap<String, ?>)bodyJson.get("Role");
	String kind_=bodyMap.get("kind").toString();
	String location_=bodyMap.get("location").toString();
	role=new Role(kind_,location_);
    }
    
    public CommFailure(String type,Role role_, String status, long timestamp,String user) {
	super(type,role_.toString(),status,timestamp,user);
        role=new Role(role_);
    }
    public String getMyKind(){return "CommFailure";}
    public String getStatus(){return status;}
    @Override
    public String toString() {
	return "{\"CommFailure\":{"+role+",\"status\":\""+status+"\",\"timestamp\":\""+timestamp+"\",\"user\":\""+user+"\"}}";
    }
    private static HashMap<?, ?> getJsonRequest(String text){
	Object jsonData = new HashMap<Object, Object>();
	jsonData=JSONUtils.decodeJSON(text);
	if(!((HashMap<?, ?>)jsonData).isEmpty()){
	    return (HashMap<?, ?>)jsonData;
	}
	return null;		
    }
}
