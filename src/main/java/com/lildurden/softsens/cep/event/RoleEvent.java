package com.lildurden.softsens.cep.event;

public class RoleEvent {

    protected Role role;
    //protected Period period;
  
    // protected boolean isBegin;
    protected String user;
    
    public RoleEvent(String type,String location/*, long beginTime,long endTime*/,String user) {
	role=new Role(type,location);
	//this.period = new Period(beginTime,endTime);
	this.user=user;
    }
    public Role getRole(){
	return role;
    }
    
    public boolean roleEquals(Role r1){
	return role.equals(r1);
    }
    public boolean roleEquals(RoleEvent re1){
	return role.equals(re1.getRole());
    }
    // public Period getPeriod(){
    // 	return period;
    // }
    
    public String getClassN(){
	return this.getClass().getName();
    }
    // public boolean getBegin(){
    // 	return period.getIsBegin();
    // }
    public String getUser(){
	return this.user;
    }
    @Override
    public String toString() {
	return "{\"RoleEvent\":"+role+"}";
    }

}
