package com.lildurden.softsens.cep;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lildurden.softsens.cep.http.GetEventsByHttp;

// import com.lildurden.softsens.cep.subscriber.manager.rule.RuleManager;
// import java.util.HashMap;
// import java.util.Map;

/**
 * Entry point for the engine.
 */
public class StartDemo {

    /** Logger */
    private static Logger LOG = LoggerFactory.getLogger(StartDemo.class);

    
    /**
     * Main method - start the engine!
     */
    public static void main(String[] args) throws Exception {

        LOG.debug("Starting...");

        //GetEventsByHttp server= new GetEventsByHttp(); 
	
	//server.startServer();

	FromFile ff=new FromFile();
	
	// System.out.println(RuleManager.write_forced("ruleT1","Contentas2 lolilol","8080"));
	
	// HashMap<String,HashMap<String,String>> rules=
	//     new HashMap<String,HashMap<String,String>>();
	// rules=RuleManager.getRules();
	// for ( Map.Entry<String,HashMap<String,String> > entry : rules.entrySet()) {
	//     String key = entry.getKey();
	//     HashMap<String,String>  tab = entry.getValue();
	//     System.out.println(key+" :");
	//     for ( Map.Entry<String,String>  entry2 : tab.entrySet()) {
	// 	String key2 = entry2.getKey();
	// 	String tab2 = entry2.getValue();
	// 	System.out.println(key2+" "+tab2);
	//     }
	// }
    }

}

/** \mainpage StreamUbiEngine : execute parsed event on rules
 * \author Adrien Carteron
 * \version  0.0.1 
 * \date 2016-07-10
 * \section intro_sec Introduction
 * Contact: acarteron@openmailbox.org
 *
 * \section install_sec Installation
 *
 * This is f***** easy
 *
 * \subsection step1 Step 1 : Compile
 *
 *  Run this command to the project root
 *  <br/>
 *  <code>mvn clean package</code>
 *
 * \subsection step2 Step 2 : Launch
 *
 *  Compiled program can be found in the subfolder <code>./bin</code>, it is called <code></code>.
 * 
 *  \subsection step3 Step 3 : launch arguments:
 *  
 *  You can read the file <code>README.RTFM</code> if it exists
 */
