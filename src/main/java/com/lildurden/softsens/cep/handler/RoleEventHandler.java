package com.lildurden.softsens.cep.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;

import com.lildurden.softsens.cep.event.SEvent;
import com.lildurden.softsens.cep.subscriber.manager.RuleStatement;
import com.lildurden.softsens.cep.subscriber.manager.Rule;

import com.lildurden.softsens.cep.util.inout.Output;
import com.espertech.esper.client.Configuration;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPServiceProviderManager;
import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.EPServiceProviderIsolated;


/**
 * This class handles Events. It processes them through the EPService, to which
 * it has attached the queries.
 */
public class RoleEventHandler{

    /** Logger */
    private static Logger LOG = LoggerFactory.getLogger(RoleEventHandler.class);
    /** Esper service */
    private EPServiceProvider epService;
    private String id;
    //List<EPLStatement> eplStatements=new ArrayList<EPLStatement>();
    private HashMap<String,
	            RuleStatement> eplStatements=new HashMap<String,
	                                                     RuleStatement>();
    public RoleEventHandler(){
	initService();
    }
    public RoleEventHandler(String id){
	LOG.info("Initializing Servcie ..");
        Configuration config = new Configuration();
	this.id=id;
        config.addEventTypeAutoName("com.lildurden.softsens.cep.event");
        epService = EPServiceProviderManager.getProvider(this.id,config);
	    //getDefaultProvider(config);
    }
    
    /**
     * Configure Esper Statement(s).
     */
    public void initService() {
        LOG.info("Initializing Servcie ..");
        Configuration config = new Configuration();
        config.addEventTypeAutoName("com.lildurden.softsens.cep.event");
        epService = EPServiceProviderManager.getDefaultProvider(config);
    }
    
    public ArrayList<String> addRule(Rule rule){
	ArrayList<String> ret=new ArrayList<String>();
	ArrayList<String> result=new ArrayList<String>();
    	if(!eplStatements.containsKey(rule.getName())){
    	    eplStatements.put(rule.getName(),new RuleStatement(epService,
							       rule));
    	    result=eplStatements.get(rule.getName()).createStatementSubscriberImplement();
    	}
    	for (int i = 0; i < result.size(); i++) {
	    if(result.get(i).contains("Subscriber error:")){
		ret.add(result.get(i));
	    }
	}
	if(ret.size()>0)
	    destroyRule(rule.getName());
    	return ret;
    }
    public ArrayList<String> addRuleForced(Rule rule){
	ArrayList<String> ret=new ArrayList<String>();
	ArrayList<String> result=new ArrayList<String>();

    	if(eplStatements.containsKey(rule.getName())){
    	    destroyRule(rule.getName());
    	}
    	eplStatements.put(rule.getName(),new RuleStatement(epService,
    							   rule));
        result=eplStatements.get(rule.getName()).createStatementSubscriberImplement();
	for (int i = 0; i < result.size(); i++) {
	    if(result.get(i).contains("Subscriber error:")){
		ret.add(result.get(i));
	    }
	}
	if(ret.size()>0)
	    destroyRule(rule.getName());
    	return ret;
    }
    public int destroyRule(String name){
	int ret=0;
	if(eplStatements.get(name)!=null){
	    if(eplStatements.get(name).destroyStatement()!=0)
		ret=-1;
	    eplStatements.remove(name);
	}else
	    ret=-2;
	return ret;
    }
    /**
     * Handle role events
     */
    public synchronized void handle(SEvent event) {
	// if(event.getRole().getType().equals("Calendar")||event.getRole().getLocation().equals("Calendar"))
	LOG.debug("add event: "+event);
        epService.getEPRuntime().sendEvent(event);
	//todo: replace by write mongo depending on config
	//Output.writeFile(event);
    }

    //@Override
    public void afterPropertiesSet() {     
        LOG.info("Configuring..");
        initService();
    }
}
