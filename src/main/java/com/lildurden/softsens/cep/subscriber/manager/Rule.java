package com.lildurden.softsens.cep.subscriber.manager;

import java.util.ArrayList;
import java.util.Arrays; 
//import java.lang.Comparable;

public class Rule implements Comparable<Rule>{
    protected String name;
    protected ArrayList<String> expression=new ArrayList<String>();
    protected String id;
    protected boolean forceWrite;

    public Rule(Rule rule){
	this.name=rule.name;
	this.expression=rule.expression;
	this.id=rule.id;
	this.forceWrite=rule.forceWrite;
    }
    public Rule(String name,ArrayList<String> expression){
	this.name=name;
	this.expression=expression;
	this.id="";
	this.forceWrite=false;	
    }
    public Rule(String name,ArrayList<String> expression,String id){
	this.name=name;
	this.expression=expression;
	this.id=id;
	this.forceWrite=false;	
    }
    public Rule(String name,ArrayList<String> expression,String id,String forceWrite){
	this.name=name;
	this.expression=expression;
	this.id=id;
	this.forceWrite=(forceWrite.equals("true")?true:false);	
    }

    
    public String getName(){return name;}
    public ArrayList<String> getExpression(){return expression;}
    public String getId(){return id;}
    public boolean getForceWrite(){return forceWrite;}
    public int compareTo(Rule rule){
	return this.name.compareTo(rule.name);
    }
}
