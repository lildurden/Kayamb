package com.lildurden.softsens.cep.subscriber.manager;

import com.lildurden.softsens.cep.util.inout.Files;
import com.lildurden.softsens.cep.subscriber.manager.Rule;
import com.lildurden.softsens.cep.util.json.JSONUtils;
import com.lildurden.softsens.cep.util.inout.MongoWrite;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Collections;

public final class RuleManager{
    private static String defaultpath="../Rouler/";
    private static String savedRulePath="/opt/webapp/rules/";

    public static int writeMongo(Rule rule){
	MongoWrite.getInstance().writeRule(rule);
	return 0;
    }
    
    //write to mongo
    public static int write(Rule rule){
	int indic=-1;
	String pathfull=savedRulePath+rule.getId()+"/"+rule.getName();
	ArrayList<String> message=rule.getExpression();
	System.out.println(pathfull);
	if(rule.getForceWrite()){
	    if(!Files.getInstance().exists(pathfull))
	    indic=0;
	    Files.getInstance().write(pathfull,message.get(0));
	}
	else{
	    if(!Files.getInstance().exists(pathfull))
		return -1;
	    Files.getInstance().writeNext(pathfull,message.get(0));
	}    
	for(int i=1;i<message.size();++i)
	    Files.getInstance().writeNext(pathfull,message.get(i));
	return indic;
    }
    
    public static int write(String name,ArrayList<String> message, String user){
	String pathfull=savedRulePath+user+"/"+name;
	if(Files.getInstance().exists(pathfull))
	    return -1;
	for(int i=0;i<message.size();++i)
	    Files.getInstance().writeNext(pathfull,message.get(i));
	return 0;
    }
    public static int write_forced(String name,ArrayList<String> message, String user){
	int indic=-1;
	String pathfull=savedRulePath+user+"/"+name;
	if(!Files.getInstance().exists(pathfull))
	    indic=0;
	Files.getInstance().write(pathfull,message.get(0));
	for(int i=1;i<message.size();++i)
	    Files.getInstance().writeNext(pathfull,message.get(i));
	return indic;
    }

     public static ArrayList<String> getGlobalRules(){
	 // ArrayList<String> rules=new ArrayList<String>();
	 // HashMap<String,String> globalRules=getUserRules(path,"global");
	 // for ( Map.Entry<String,String>  rule :
	 // 	   globalRules.entrySet()) {
	 //     //String  = rule.getKey();
	 //     rules.add(rule.getValue());
	 // }
	 // return rules;
	 return null;
    }
    
    private static HashMap<String,ArrayList<Rule>> getRules(String path){
	HashMap<String,ArrayList<Rule>> usersRules=
	    new HashMap<String,ArrayList<Rule>>();
	List<String> directoriesNames = new ArrayList<String>();
	directoriesNames=Files.getInstance().getFolderContents(path,false);
	for(int i=0;i<directoriesNames.size();++i){
	    usersRules.put(directoriesNames.get(i),
			   getUserRules(path,directoriesNames.get(i)));
	}
	return usersRules;
    }

    public static ArrayList<Rule> getDDBRules(){
	return MongoWrite.getInstance().readRules();
	// HashMap<String,ArrayList<Rule>> defaultRules=new HashMap<String,ArrayList<Rule>>();
	// return defaultRules;
    }
    
    public static HashMap<String,ArrayList<Rule>> getRules(){
	//HashMap<String,ArrayList<Rule>> savedRules=getRules(savedRulePath);
	HashMap<String,ArrayList<Rule>> defaultRules=getRules(defaultpath);
	//defaultRules.putAll(savedRules);
	return defaultRules;
    }
    
    private static ArrayList<Rule> getUserRules(String path,String user){
	path+=user+"/";
	//System.out.println(path);
	ArrayList<Rule> rules=new ArrayList<Rule>();	
	List<String> rulesFiles = new ArrayList<String>();
	rulesFiles = Files.getInstance().getFolderContents(path);
	for (int i=0; i<rulesFiles.size();++i){
	    if(rulesFiles.get(i).indexOf("~")==-1){
		
		// System.out.println(rulesFiles.get(i).
		// 		   substring(0,
		// 			     rulesFiles.get(i).length()-4));
		// System.out.println(Files.getInstance().read(path+rulesFiles.get(i)));

		ArrayList<String> rule=new ArrayList<String>(Files.getInstance().read(path+rulesFiles.get(i)));
		if(rule!=null&&rule.size()!=0){
		    rules.add(new Rule(rulesFiles.get(i).
				       substring(0,
						 rulesFiles.get(i).length()),
				       rule,
				       user));
		}
	    }
	}
	Collections.sort(rules);
	return rules;
    }
    public static Rule setRule(HashMap<?, ?> sourceMap){
	String user=sourceMap.get("user")!=null?sourceMap.get("user").toString():"";
	String name= sourceMap.get("name")!=null?sourceMap.get("name").toString():"";
	String forceWrite= sourceMap.get("force_write")!=null?sourceMap.get("force_write").toString():"";
	Object expression=sourceMap.get("expression")!=null?JSONUtils.decodeFromMap(sourceMap.get("expression")):null;
	ArrayList<String>exp= new ArrayList<String>((LinkedList<String>)expression);	
	return new Rule(name,exp,user,forceWrite);
    }
    
}
