package com.lildurden.softsens.cep.subscriber.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Map;
import java.util.Iterator;
import java.util.Set;
import com.lildurden.softsens.cep.util.inout.Output;
import com.lildurden.softsens.cep.event.StreamEvent;
import com.lildurden.softsens.cep.subscriber.impl.StatementSubscriberBasic;


public class StatementSubscriberImplement extends StatementSubscriberBasic {

    private static Logger LOG = LoggerFactory.getLogger(StatementSubscriberImplement.class);

    
    /**
     * Listener method called when Esper has detected a pattern match.
     */
    public void update(Map<String, Object> eventMap) {
	LOG.debug(name+" impl \n"+eventMap.toString()+"\n");
	//Output.writeFile(eventMap,name);
	Output.BsonRuleSaveResults(eventMap,name);

    }    
}

