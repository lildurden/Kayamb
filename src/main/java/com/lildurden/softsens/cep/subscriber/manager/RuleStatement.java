package com.lildurden.softsens.cep.subscriber.manager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

import com.lildurden.softsens.cep.subscriber.impl.StatementSubscriberImplement;
import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPServiceProviderManager;
import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.EPException;

public class RuleStatement extends Rule{
    private static Logger LOG = LoggerFactory.getLogger(RuleStatement.class);
    private ArrayList<EPLStatement> eplStatements=
	new ArrayList<EPLStatement>();

    private void initRules(EPServiceProvider epService){
	for(int i=0;i<expression.size()-1;++i){
	    eplStatements.add(new EPLStatement(epService,
					       name,
					       expression.get(i)));
	    
	}
	eplStatements.add(new EPLStatement(epService,
					   name,
					   expression.get(expression.size()-1),
					   true));
    }
    
    public RuleStatement(Rule rule){
	super(rule);
    }
    public RuleStatement(EPServiceProvider epService,Rule rule){
	super(rule);
	initRules(epService);
    }
    public ArrayList<String> createStatementSubscriberImplement(){
	ArrayList<String> resStatements=new ArrayList<String>();
	for(int i=0;i<eplStatements.size();++i ){
	    resStatements.add(eplStatements.get(i).createStatementSubscriberImplement());
	}
	return resStatements;
    }
    public int destroyStatement(){
	boolean issue=false;
	for(int i=0;i<eplStatements.size();++i ){
	    if(eplStatements.get(i).destroyStatement()!=0)
		issue=true;
	}
	if(issue)
	    return -1;
	return 0;
    }
}
