package com.lildurden.softsens.cep.subscriber.manager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lildurden.softsens.cep.subscriber.impl.StatementSubscriberBasic;
import com.lildurden.softsens.cep.subscriber.impl.StatementSubscriberImplement;

import com.espertech.esper.client.EPServiceProvider;
import com.espertech.esper.client.EPServiceProviderManager;
import com.espertech.esper.client.EPStatement;
import com.espertech.esper.client.EPException;

public class EPLStatement{
    private static Logger LOG = LoggerFactory.getLogger(EPLStatement.class);

    
    private StatementSubscriberBasic statementSubscriberImplementSubscriber;
    private EPStatement statementSubscriberImplementEPS;
    private EPServiceProvider epService;
    
    
    public EPLStatement(EPServiceProvider epService,String name,String expression){
	this.epService=epService;
	statementSubscriberImplementSubscriber=
	    new StatementSubscriberBasic();
	statementSubscriberImplementSubscriber.setStatement(name,expression);
    }
    public EPLStatement(EPServiceProvider epService,String name,String expression,boolean choosimpl){
	this.epService=epService;
	if(choosimpl){
	    statementSubscriberImplementSubscriber=
		new StatementSubscriberImplement();
	}else{
	    statementSubscriberImplementSubscriber=
		new StatementSubscriberBasic();
	}
	statementSubscriberImplementSubscriber.setStatement(name,expression);
    }
    public String createStatementSubscriberImplement(){
    	LOG.info("create "
		  +statementSubscriberImplementSubscriber.getName());
	try{
	    statementSubscriberImplementEPS = epService.getEPAdministrator().createEPL(statementSubscriberImplementSubscriber.getStatement());
        
	}catch(EPException e){
	    LOG.error("Subscriber error: "+e);
	    return "Subscriber error: "+e;
	}
	statementSubscriberImplementEPS.setSubscriber(statementSubscriberImplementSubscriber);
	LOG.info("This Is done "+statementSubscriberImplementEPS.getName()+" "+statementSubscriberImplementEPS.getText());
	return statementSubscriberImplementEPS.getName();
    }
    
    public int destroyStatement(){
	if(statementSubscriberImplementEPS!=null){
	    LOG.info("Statement "+statementSubscriberImplementEPS.getName()+" destroyed");
	    statementSubscriberImplementEPS.destroy();
	    return 0;
	}else
	    return -1;
    }
}
