package com.lildurden.softsens.cep.subscriber.impl;

import java.util.Map;
import java.util.Iterator;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.lildurden.softsens.cep.event.StreamEvent;
import com.lildurden.softsens.cep.subscriber.StatementSubscriber;


public class StatementSubscriberBasic implements StatementSubscriber {

    private static Logger LOG = LoggerFactory.getLogger(StatementSubscriberBasic.class);

    protected String expression="";
    protected String name="";

    public void setStatement(String name,String expression){
	this.expression=expression;
	this.name=name;
    }
    public String getName(){
	return name;
    }
    
    public String getStatement() {	
        return expression;
    }
    
    /**
     * Listener method called when Esper has detected a pattern match.
     */
    public void update(Map<String, Object> eventMap) {
	LOG.debug(name+" basic \n"+eventMap.toString()+"\n");
    }    
}
